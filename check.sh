#!/bin/bash

ERR=0

if [[ -d formatpatches ]]; then
    rm -rf formatpatches
fi

for file in src/*; do
    if [[ ! "$(cat $file)" =~ ^"$(cat LICENSE)" ]]; then
        echo "Missing or incorrect license comment in \"$file\""
        ERR=1
    fi

    if [[ ! "$file" =~ ^src/.*\.[ch]$ ]]; then
        echo "\"$file\" does not follow the naming standards"
        ERR=1
    fi

    if [[ "$file" =~ \.c$ ]]; then
        path=$(echo "$file" | sed 's/\/[^/]*$//g')
        mkdir -p "formatpatches/$path"
        cat "$file" | ./format.pl > "formatpatches/$file"

        cmp -s "formatpatches/$file" "$file" && rm "formatpatches/$file"
    fi
done

if [[ -d "formatpatches" ]]; then
    echo "Recommended formatting changes are written to formatpatches"
    echo "To apply the changes, run \`vimdiff original formatpatches/formated\`"
    echo ""
    echo "Formatted files:"
    find formatpatches -type f | sort
    echo ""
    ERR=1
fi

if [[ $ERR == 0 ]]; then
    echo "No problems found"
fi

exit $ERR
