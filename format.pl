#!/bin/perl

use strict;
use warnings;

my $debugging = 0;
my $dbgd = 0;

sub dbg {
    if ($debugging) {
        print $_[0];
        $dbgd += length($_[0]);
    }
}

sub margin {
    if ($debugging) {
        return 20 - $dbgd;
    } else {
        return 0;
    }
}

sub max {
    return $_[0] > $_[1] ? $_[0] : $_[1];
}

my @prntBrktIndexs = ();

sub printLine {
    my $line = $_[0];
    my $indent = $_[1];
    my $_indent = $indent;

    my $lastDiv = 0;
    my $i = 0;

    if (length($line) == 0) {
        return;
    }

    print " " x $indent;

    while ($i < length($line)) {
        my $char = substr($line, $i, 1);
        my $nextChar = " ";
        my $prevChar = " ";

        if ($i > 0) {
            $prevChar = substr($line, $i-1, 1);
        }

        if ($i < length($line) - 1) {
            $nextChar = substr($line, $i+1, 1);
        }

        if ($char =~ m/[\{,\*\/\+\-;=]/ && $prevChar =~ m/[\sa-zA-Z0-9\]\)\}]/ && $nextChar =~ m/[\s0-9\[\(\{]/) {
            $lastDiv = $i;
        }

        if ($char eq "(") {
            push @prntBrktIndexs, $i + $_indent + 1;
        } elsif ($char eq ")") {
            pop @prntBrktIndexs;
        }

        if ($lastDiv && $i + $_indent >= 80) {
            print substr($line, 0, $lastDiv + 1);
            print "\n";

            substr($line, 0, $lastDiv + 1) = "";
            ($line = $line) =~ s/^\s*//g;

            $_indent = @prntBrktIndexs ? $prntBrktIndexs[-1] : $indent;# + 4;

            my $index = 0;
            while ($_indent > $lastDiv + $indent && scalar(@prntBrktIndexs) > - $index) {
                $index--;
                $_indent = $prntBrktIndexs[$index];
            }

            if (length($line)) {
                print " " x $_indent;
            }

            $i = 0;
            $lastDiv = 0;
        } else {
            $i++;
        }
    }

    if (length($line)) {
        print "$line\n";
    }
}

my $mlc = 0;
my $mlcIndent = 0;
my $equalIndent = 0;
my $preproc = 0;
my $define = 0;
my $scope = 0;
my $ifIndent = 0;
my @brktIndexs = ();
my $brktIndent = 0;

while (my $line = <>) {
    chomp $line;
    $line =~ s/^\s+|\s$//;

    my $text = $line;

    my $slash = 0;
    my $newlinebrace = 0;

    my $scopeInc = 0;
    my $scopeDec = 0;

    my $instring = 0;
    my $mlcStart = -1;

    my $equalIndex = 0;

    my $caseIndent = 0;

    $dbgd = 0;

    my $i = -1;
    while ($i < length($text)) {
        $i++;
        my $char = substr($text, $i, 1);

        if (!$mlc && $char eq "\"") {
            $instring = !$instring;
        } elsif ($instring) {
            next;
        }

        if ($mlc) {
            if ($char eq "/" && substr($text, $i-1, 1) eq "\*") {
                $mlc = 0;
                if ($mlcStart != -1) {
                    substr($text, $mlcStart-1, $i-$mlcStart+2) = "";
                    $mlcStart = -1;
                }
            }
            next;
        }

        if ($char eq "/") {
            $slash++;
            if ($slash == 2) {
                $text = substr($text, 0, $i-1);
                $text =~ s/^\s+|\s$//;
                $slash = 0;
                last;
            }
        } elsif ($char eq "\*" && $slash == 1) {
            $mlc = 1;
            $mlcStart = $i;
        } else {
            $slash = 0;
        }

        if ($char eq "(") {
            push @brktIndexs, $i + 1;
        } elsif ($char eq ")") {
            pop @brktIndexs;
        } elsif ($char eq "=") {
            $equalIndex = $i;
        }
    }

    if ($text =~ m/{\s*\\*$/) {
        $scopeInc++;
    }
    if ($text =~ m/^\s*}/) {
        $scopeDec++;
    }

    my $preprocIndent = 0;

    if ($line =~ m/^#/) {
        ($line = $line) =~ s/^#\s*/#/;

        if ($line =~ m/^#if/) {
            substr($line, 1, 0) = " " x max(4 * $preproc - 1, 0);
            $preproc++;
        } elsif ($line =~ m/^#else/ || $line =~ m/^#elif/) {
            substr($line, 1, 0) = " " x max(4 * ($preproc - 1) - 1, 0);
        } elsif ($line =~ m/^#endif/) {
            $preproc--;
            substr($line, 1, 0) = " " x max(4 * $preproc - 1, 0);
        } else {
            substr($line, 1, 0) = " " x max(4 * $preproc - 1, 0);
        }
    } elsif ($preproc) {
        $preprocIndent = $preproc * 4;
    }

    if ($text =~ m/\(/ && $text =~ m/\{$/ && $text !~ m/^}*\s*(if|else|while|for|switch)/) {
        chop $text;
        $newlinebrace = 1;
    }

    if ($text =~ m/(case|default).*:/ && $scope) {
        $caseIndent = -4;
    }

    my $ifdiffindent = 0;
    if ($text =~ m/^if.*\)\s*\\*$/) {
        $ifdiffindent = 1;
    }

    $scope -= $scopeDec;

    dbg("$equalIndex,$brktIndent");

    if ($debugging) {
        print " " x margin;
        print "|";
    }

    my $indent;
    if ($text =~ m/^#/) {
        $indent = 0;
    } else {
        $indent = $scope * 4 + $define + max($preprocIndent-4, 0) + $brktIndent
               + $mlcIndent + $equalIndent + $ifIndent + $caseIndent;
    }

    if (length($line) == 0) {
        print "\n";
    } elsif ($newlinebrace) {
        $line = substr($line, 0, length($line)-1);
        $line =~ s/\s*$//;
        printLine($line, $indent);

        if ($debugging) {
            print " " x 20;
            print "|";
        }

        print " " x $indent;
        print "{";
        print "\n";
    } else {
        printLine($line, $indent);
    }

    $scope += $scopeInc;

    if (@brktIndexs) {
        $brktIndent = $brktIndexs[-1];
    } else {
        $brktIndent = 0;
    }

    $mlcIndent = $mlc;

    if ($line =~ m/^#\s*define/) {
        $define = 4;
    }

    if ($line =~ m/[^\\]$/ || $line eq "") {
        $define = 0;
    }

    if ($text =~ m/[;\{]\s*\\*$/) {
        $equalIndent = 0;
    } elsif ($equalIndex && !$brktIndent && $text !~ m/^}*\s*(if|else|while|for|switch)/) {
        $equalIndent = 4;
    }

    if ($ifdiffindent || $ifIndent && ($scopeInc || $scopeDec)) {
        $ifIndent += 4;
    } else {
        $ifIndent = 0;
    }
}
