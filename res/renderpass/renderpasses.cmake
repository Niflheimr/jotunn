file(GLOB RENDERPASS_SOURCES "res/renderpass/*.json")

foreach(RENDERPASS_SRC ${RENDERPASS_SOURCES})
    get_filename_component(FILE_NAME ${RENDERPASS_SRC} NAME)
    set(OBJ "${CMAKE_SOURCE_DIR}/CMakeFiles/jotunn.dir/${FILE_NAME}.o")
    add_custom_command(
        OUTPUT ${OBJ}
        WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}/res/renderpass/
        COMMAND ld -r -b binary -o ${OBJ} ${FILE_NAME}
        DEPENDS ${RENDERPASS_SRC})
    list(APPEND RENDERPASSES ${OBJ})
endforeach(RENDERPASS_SRC)

add_custom_target(renderpasses DEPENDS ${RENDERPASSES})
