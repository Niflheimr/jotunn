file(GLOB PIPELINE_SOURCES "res/pipeline/*.json")

foreach(PIPELINE_SRC ${PIPELINE_SOURCES})
    get_filename_component(FILE_NAME ${PIPELINE_SRC} NAME)
    set(OBJ "${CMAKE_SOURCE_DIR}/CMakeFiles/jotunn.dir/${FILE_NAME}.o")
    add_custom_command(
        OUTPUT ${OBJ}
        WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}/res/pipeline/
        COMMAND ld -r -b binary -o ${OBJ} ${FILE_NAME}
        DEPENDS ${PIPELINE_SRC})
    list(APPEND PIPELINES ${OBJ})
endforeach(PIPELINE_SRC)

add_custom_target(pipelines DEPENDS ${PIPELINES})
