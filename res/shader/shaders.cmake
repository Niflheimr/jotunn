#Command names
set(SPIRV_COMPILER glslangValidator)

file(GLOB GLSL_SOURCES "res/shader/*.frag" "res/shader/*.vert")

foreach(GLSL ${GLSL_SOURCES})
    get_filename_component(FILE_NAME ${GLSL} NAME)
    set(SPIRV "${FILE_NAME}")
    add_custom_command(
        OUTPUT ${SPIRV}
        WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}/CMakeFiles/jotunn.dir
        COMMAND ${SPIRV_COMPILER} -V -o ${SPIRV} ${GLSL}
        DEPENDS ${GLSL})
    set(OBJ "${CMAKE_SOURCE_DIR}/CMakeFiles/jotunn.dir/${FILE_NAME}.o")
    add_custom_command(
        OUTPUT ${OBJ}
        WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}/CMakeFiles/jotunn.dir
        COMMAND ld -r -b binary -o ${FILE_NAME}.o ${SPIRV}
        DEPENDS ${SPIRV})
    list(APPEND SHADERS ${OBJ})
endforeach(GLSL)

add_custom_target(shaders DEPENDS ${SHADERS})
