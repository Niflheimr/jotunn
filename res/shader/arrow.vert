#version 450

layout(set = 0, binding = 0) uniform JECamera {
    mat4 PV;
} camera;

layout(push_constant) uniform PushConstants
{
    vec3 points[2];
    vec3 colors[2];
} pushConstants;

layout(location = 0) out vec3 outColor;

out gl_PerVertex
{
    vec4 gl_Position;
};

void main()
{
    gl_Position = camera.PV * vec4(pushConstants.points[gl_VertexIndex], 1.0);
    outColor = pushConstants.colors[gl_VertexIndex];
}
