Coding Guidelines
=================

All the code inside this project must comply with the following rules:

#### File Names

All source files have to start with JE, short for Jǫtunn Engine, followed by
the actual name, always using upper camel case.

```c
    #include "JECore.h"
    #include "JENewHeaderFile.h"
```

#### Variables And Structs

Structs will use upper camel case and variables will use lower camel case.
Avoid whenever possible the use of typedef for structures.

```c
    struct ThisIsAStructType {
        int x;
        int y;
        int z;
    };

    struct ThisIsAStructType thisIsAStruct;

    int thisIsANumber;
```

#### Preprocessor

Macros and defines will use uppercase characters separated by underscores.
For if statements using the preprocessor, use indentation but leave the "#" at
the start of the line.

```c
    #define CONSTANT_VALUE 0x5C

    #ifdef SOMETHING
    #   include <something.h>
    #endif
```

#### Functions

Functions will use the pattern jeModuleNameOfTheFunction(), where module is a
single word that remotely indicates what part of the engine the function
corresponds to, usually the same as the file name and the whole name uses
lower camel case. The curly brackets defining the function block will be on a
new line. Static functions do not need to follow the name pattern since they are
not "exposed" to the outside (as an API function) but they do have to use lower
camel case.

```c
    void jeAudioPlayFile(const char *file);

    void jeWindowCreate()
    {
        //The function content
    }

    //Inside a .c file
    static void thisIsAnInternalFunction()
    {

    }
```

#### Misc

Pointers will have the * at the variable name side. The if clause parenthesis
containing the expression should be separated of the brackets and the if/else.
Same goes for while and for loops. Mathematical operations will be separated by
spaces.

```c
    char *var;
    int *a, *b, *c;

    if (someValue > someOtherValue) {
        doSomething();
    } else if (someOtherValue == 5) {
        doSomethingElse();
    } else {
        doSomethingDifferent();
    }

    for (int i = 0; i < someValue; i++) {

    }

    while (someOtherValue > 2) {
        someOtherValue = someValue + someOtherValue - 10;
    }
```
