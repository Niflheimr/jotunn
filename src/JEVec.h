/* Copyright (C) 2017-2018 Sam Bazley and Arnau Bigas
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef JEVEC_H
#define JEVEC_H

#include <math.h>

typedef union jeVec2f {
    struct {float x, y;};
    struct {float v, t;};
} jeVec2f;

typedef union jeVec2i {
    struct {int x, y;};
    struct {int v, t;};
} jeVec2i;

typedef union jeVec3f {
    struct {float x, y, z;};
    struct {float r, g, b;};
    float val[3];
} jeVec3f;

typedef union jeVec4f {
    struct {float x, y, z, w;};
    struct {float r, g, b, a;};
    float val[4];
} jeVec4f;

static inline float jeVec3Length(jeVec3f v)
{
    return sqrtf((v.x * v.x) + (v.y * v.y) + (v.z * v.z));
}

static inline jeVec3f jeVec3Divide(jeVec3f v, float f)
{
    return (jeVec3f) {.x = v.x / f, .y = v.y / f, .z = v.z / f};
}

static inline jeVec3f jeVec3Normalize(jeVec3f v)
{
    float length = jeVec3Length(v);
    return jeVec3Divide(v, length);
}

static inline float jeVec3Dot(jeVec3f a, jeVec3f b)
{
    return (a.x * b.x) + (a.y * b.y) + (a.z * b.z);
}

static inline jeVec3f jeVec3Cross(jeVec3f a, jeVec3f b)
{
    return (jeVec3f) { .x = a.y * b.z - b.y * a.z,
        .y = b.x * a.z - a.x * b.z,
        .z = a.x * b.y - b.x * a.y};
}

static inline jeVec3f jeVec3Sub(jeVec3f a, jeVec3f b)
{
    return (jeVec3f) { .x = a.x - b.x, .y = a.y - b.y, .z = a.z - b.z };
}

static inline jeVec3f jeVec3Add(jeVec3f a, jeVec3f b)
{
    return (jeVec3f) { .x = a.x + b.x, .y = a.y + b.y, .z = a.z + b.z };
}

#endif /* JEVEC_H */
