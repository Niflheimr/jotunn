/* Copyright (C) 2017-2018 Sam Bazley and Arnau Bigas
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */


#include "JELogger.h"

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <time.h>
#include <string.h>

#define MAX_LOG_LENGTH 1024

int jeErrorIsSet(JEError *err)
{
    return err->line > 0;
}

void jeErrorFree(JEError *err)
{
    free(err->msg);
}

static uint8_t flags;

void jeLoggerFlags(uint8_t t_flags)
{
    flags = t_flags;
}

static void getTime(char *buffer)
{
    time_t rawtime = time(NULL);
    struct tm *timeinfo = localtime(&rawtime);
    strftime(buffer, 11, "[%H:%M:%S]", timeinfo);
}

//TODO: Log to a file
static void output(FILE *stream, const char *type, const char *msg)
{
    char timebuf[11];
    getTime(timebuf);
    fprintf(stream, "%s%s %s\n", timebuf, type, msg);
}

void jeLoggerDebug(const char *msg, ...)
{
    if (!(flags & JE_LOG_DEBUG)) return;
    va_list argptr;
    va_start(argptr, msg);
    char buffer[MAX_LOG_LENGTH];
    vsprintf(buffer, msg, argptr);
    va_end(argptr);
    output(stdout, "[DBG ]", buffer);
}

void jeLoggerWarning(const char *msg, ...)
{
    if (!(flags & JE_LOG_WARNING)) return;
    va_list argptr;
    va_start(argptr, msg);
    char buffer[MAX_LOG_LENGTH];
    vsprintf(buffer, msg, argptr);
    va_end(argptr);
    output(stdout, "[WARN]", buffer);
}

void jeLoggerInfo(const char *msg, ...)
{
    va_list argptr;
    va_start(argptr, msg);
    char buffer[MAX_LOG_LENGTH];
    vsprintf(buffer, msg, argptr);
    va_end(argptr);
    output(stdout, "[INFO]", buffer);
}

void jeLoggerError(JEError *err)
{
    char msg[MAX_LOG_LENGTH];
    sprintf(msg, "%s: line %d in function %s:\n%s", err->file, err->line,
            err->func, err->msg);
    output(stderr, "[ERR ]", msg);
}
