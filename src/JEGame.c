/* Copyright (C) 2017-2018 Sam Bazley and Arnau Bigas
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include "JEGame.h"
#include "JETimer.h"
#include "JELogger.h"
#include "JEEvent.h"
#include <stdlib.h>
#include "JERender.h"

#define TICKRATE 60

static int argc;
static char **argv;

static int running;

typedef struct JEFuncEntry {
    void (*func)();
    struct JEFuncEntry *next;
} JEFuncEntry;

static JEFuncEntry *firstUpdateEnt = 0, *currentUpdateEnt;
static JEFuncEntry *firstRenderEnt = 0, *currentRenderEnt;

void jeGameInit(int _argc, char **_argv, JEWindowProperties *windowProperties,
                JEError *err)
{
    static int initialised = 0;
    if (initialised) return;
    initialised = 1;

    argc = _argc;
    argv = _argv;

    _jeWindowCreate(windowProperties, err);

    _jeRenderInit(windowProperties->title, 0, err);
}

int jeGameGetArgc()
{
    return argc;
}

char **jeGameGetArgv()
{
    return argv;
}

void jeGameStart()
{
    JETimer tickTmr;
    JETimer secTmr;

    running = 1;

    int nextTick = 0;
    int nextSecond = 1000000;

    long ticks = 0;
    long renders = 0;

    jeTimerStart(&tickTmr);
    jeTimerStart(&secTmr);

    while (running) {

        if (jeTimerElapsedMicroseconds(&tickTmr) >= nextTick) {
            nextTick += 1000000/TICKRATE-jeTimerElapsedMicroseconds(&tickTmr);
            jeTimerStart(&tickTmr);
            ticks++;

            JEFuncEntry *fe = firstUpdateEnt;
            while (fe) {
                fe->func();
                fe = fe->next;
            }
        }

        _jeWindowPollEvents();

        renders++;

        JEFuncEntry *fe = firstRenderEnt;
        while (fe) {
            fe->func();
            fe = fe->next;
        }

        _jeRenderDraw();

        if (jeTimerElapsedMicroseconds(&secTmr) >= nextSecond) {
            nextSecond += 1000000-jeTimerElapsedMicroseconds(&secTmr);
            jeTimerStart(&secTmr);

            jeLoggerInfo("TPS: %u, FPS: %u", ticks, renders);

            ticks = renders = 0;
        }
    }

    while (firstUpdateEnt) {
        JEFuncEntry *next = firstUpdateEnt->next;
        free(firstUpdateEnt);
        firstUpdateEnt = next;
    }
    while (firstRenderEnt) {
        JEFuncEntry *next = firstRenderEnt->next;
        free(firstRenderEnt);
        firstRenderEnt = next;
    }

    _jeRenderCleanup();

    _jeWindowCleanup();

    _jeRenderCleanupFinal();
}

void jeGameStop()
{
    running = 0;
}

void jeGameAddUpdateHook(void (*func)())
{
    JEFuncEntry *new = malloc(sizeof(JEFuncEntry));
    if (firstUpdateEnt) {
        currentUpdateEnt->next = new;
        currentUpdateEnt = new;
    } else {
        currentUpdateEnt = firstUpdateEnt = new;
    }

    currentUpdateEnt->func = func;
    currentUpdateEnt->next = 0;
}

void jeGameAddRenderHook(void (*func)())
{
    JEFuncEntry *new = malloc(sizeof(JEFuncEntry));
    if (firstRenderEnt) {
        currentRenderEnt->next = new;
        currentRenderEnt = new;
    } else {
        currentRenderEnt = firstRenderEnt = new;
    }

    currentRenderEnt->func = func;
    currentRenderEnt->next = 0;
}
