/* Copyright (C) 2017-2018 Sam Bazley and Arnau Bigas
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef JEWINDOW_H
#define JEWINDOW_H

#include "JELogger.h"

#define JE_WINDOW_RESIZABLE   (1L<<0)
#define JE_WINDOW_FULLSCREEN  (1L<<1)
#define JE_WINDOW_GRAB_CURSOR (1L<<2)

typedef struct JEWindowProperties {
    int width;
    int height;
    long flags;
    char *title;
} JEWindowProperties;

void _jeWindowPollEvents();

/**
 * Creates the window.
 * @param wp A JEWindowProperties struct. All values must be set.
 * @param err A JEError. Is set if the window creation fails.
 */
void _jeWindowCreate(JEWindowProperties *wp, JEError *err);

void _jeWindowCleanup();

typedef struct JEWindow {
    struct _XDisplay *display;
    unsigned long window;
} JEWindow;

JEWindow _jeWindowHandle();

void jeWindowUpdateFlags(long enable, long disable);
int jeWindowIsFlagSet(long flag);

void jeWindowSetTitle(char *ntitle);
void jeWindowSetDimensions(int nwidth, int nheight);

int jeWindowGetWidth();
int jeWindowGetHeight();
#endif /* JEWINDOW_H */
