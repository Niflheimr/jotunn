/* Copyright (C) 2017-2018 Sam Bazley and Arnau Bigas
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include "JESwapchain.h"

#include "JEVkUtil.h"
#include "JECamera.h"
#include "JEFramebuffer.h"

static VkSwapchainKHR swapchain = VK_NULL_HANDLE;

static VkSurfaceKHR surface;
static VkSurfaceFormatKHR surfaceFormat;

static uint32_t minSwapchainImageCount = 0;
static uint32_t imageCount;
static VkImage *images;
static VkImageView *imageViews;
static VkFence *presentFences;

static uint32_t presentModesCount = 0;
static VkPresentModeKHR *presentModes;

static VkImageUsageFlags imageUsageFlags;
static VkSurfaceTransformFlagBitsKHR transformFlags;

VkFormat _jeSwapchainFormat()
{
    return surfaceFormat.format;
}

void _jeSwapchainInitSurface(JEWindow window, VkInstance instance, JEError *err)
{
    VkResult result;
#ifdef _WIN32

#elif __linux__
    VkXlibSurfaceCreateInfoKHR surfaceInfo = {
        VK_STRUCTURE_TYPE_XLIB_SURFACE_CREATE_INFO_KHR,
        NULL,
        0,
        window.display,
        window.window
    };
    result = vkCreateXlibSurfaceKHR(instance, &surfaceInfo, NULL, &surface);
#endif
    if (result != VK_SUCCESS) {
        JE_VKERROR(err, "Couldn't create vulkan surface", result);
        return;
    }
}

int _jeSwapchainQueueCanPresent(VkPhysicalDevice device, int index)
{
    VkBool32 supportsPresentation = 0;
    vkGetPhysicalDeviceSurfaceSupportKHR(device, index, surface,
                                         &supportsPresentation);
    return supportsPresentation == VK_TRUE;
}

void _jeSwapchainInit(VkDevice device, VkPhysicalDevice physicalDevice,
                      JEError *err)
{
    VkResult result;

    //Get surface capabilities
    VkSurfaceCapabilitiesKHR surfaceCapabilities;
    result = vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice,
                                                       surface,
                                                       &surfaceCapabilities);
    if (result != VK_SUCCESS) {
        JE_VKERROR(err, "Failed to retrieve surface capabilities", result);
        return;
    }

    //Make sure we don't use more than the maximum ammount of images
    minSwapchainImageCount = surfaceCapabilities.minImageCount + 1;
    if (surfaceCapabilities.maxImageCount > 0 &&
        minSwapchainImageCount > surfaceCapabilities.maxImageCount) {
        minSwapchainImageCount = surfaceCapabilities.maxImageCount;
    }

    //Make sure we can display colour (why would we need to do that vulkan?)
    if (surfaceCapabilities.supportedUsageFlags &
        VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT) {
        imageUsageFlags = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
    } else {
        JE_ERROR(err, "VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT not supported!");
        return;
    }

    //Don't apply any transforms (again why would you want that)
    if (surfaceCapabilities.supportedTransforms &
        VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR) {
        transformFlags = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
    } else {
        transformFlags = surfaceCapabilities.currentTransform;
    }

    //Get supported surface formats

    uint32_t formatCount = 0;
    result = vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface,
                                                  &formatCount, NULL);
    if (result != VK_SUCCESS) {
        JE_VKERROR(err, "Failed to count device surface formats", result);
        return;
    } else if(formatCount == 0) {
        JE_ERROR(err, "Device surface formats count is 0");
        return;
    }

    VkSurfaceFormatKHR surfaceFormats[formatCount];
    result = vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface,
                                                  &formatCount, surfaceFormats);
    if (result != VK_SUCCESS) {
        JE_VKERROR(err, "Failed acquiring surface formats", result);
        return;
    }

    if (formatCount == 1 && surfaceFormats[0].format == VK_FORMAT_UNDEFINED) {
        //We can choose whatever format we want
        surfaceFormat.format = VK_FORMAT_R8G8B8A8_UNORM;
        surfaceFormat.colorSpace = VK_COLORSPACE_SRGB_NONLINEAR_KHR;
    } else {
        //Have to find one that suits our needs
        int formatFound = 0;
        for (int i = 0; i < formatCount; i++) {
            if (surfaceFormats[i].format == VK_FORMAT_R8G8B8A8_UNORM) {
                surfaceFormat = surfaceFormats[i];
                formatFound = 1;
                break;
            }
        }

        //No suitable format found, use the first one available
        if (!formatFound) {
            surfaceFormat = surfaceFormats[0];
        }
    }

    result = vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface,
                                                      &presentModesCount, NULL);
    if (result != VK_SUCCESS) {
        JE_VKERROR(err, "Failed to count present modes", result);
        return;
    } else if (presentModesCount == 0) {
        JE_ERROR(err, "Presentation mode count is 0");
        return;
    }

    //Get available presentation modes
    presentModes = malloc(sizeof(VkPresentModeKHR) * presentModesCount);
    result = vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface,
                                                       &presentModesCount,
                                                       presentModes);
    if (result != VK_SUCCESS) {
        JE_VKERROR(err, "Failed to acquire presentation modes", result);
        return;
    }

    //Create fence for waiting on vblank if vsync is enabled
    VkFenceCreateInfo fenceInfo = {
        VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
        NULL,
        VK_FENCE_CREATE_SIGNALED_BIT //Start signaled to commence rendering
    };

    presentFences = malloc(sizeof(VkFence) * imageCount);

    for (int i = 0; i < minSwapchainImageCount; i++) {
        vkCreateFence(device, &fenceInfo, 0, &presentFences[i]);
    }
}

static int hasPresentMode(VkPresentModeKHR mode)
{
    for (int i = 0; i < presentModesCount; i++) {
        if (presentModes[i] == mode) return 1;
    }
    return 0;
}

static void destroySwapchainImageViews(VkDevice device)
{
    if (swapchain != VK_NULL_HANDLE) {
        for (int i = 0; i < imageCount; i++) {
            vkDestroyImageView(device, imageViews[i], NULL);
        }

        free(images);
        free(imageViews);
    }
}

void _jeSwapchainCreate(int width, int height, VkDevice device,
                        VkPhysicalDevice physicalDevice, int vsync,
                        JEError *err)
{
    destroySwapchainImageViews(device);

    VkResult result;

    VkSurfaceCapabilitiesKHR surfaceCapabilities;
    result = vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice,
                                                       surface,
                                                       &surfaceCapabilities);
    if (result != VK_SUCCESS) {
        JE_VKERROR(err, "Failed to retrieve surface capabilities", result);
        return;
    }

    VkExtent2D extent = {width, height};

    //Make sure we fit in the limits
    if (surfaceCapabilities.currentExtent.width == UINT32_MAX) {
        if (extent.width < surfaceCapabilities.minImageExtent.width) {
            extent.width = surfaceCapabilities.minImageExtent.width;
        }

        if (extent.height < surfaceCapabilities.minImageExtent.height) {
            extent.height = surfaceCapabilities.minImageExtent.height;
        }

        if (extent.width > surfaceCapabilities.maxImageExtent.width) {
            extent.width = surfaceCapabilities.maxImageExtent.width;
        }

        if (extent.height > surfaceCapabilities.maxImageExtent.height) {
            extent.height = surfaceCapabilities.maxImageExtent.height;
        }
    } else {
        extent = surfaceCapabilities.currentExtent;
    }

    VkPresentModeKHR presentMode;
    int presentModeSet = 0;

    if (vsync) {
        if (hasPresentMode(VK_PRESENT_MODE_MAILBOX_KHR)) {
            presentMode = VK_PRESENT_MODE_MAILBOX_KHR;
            presentModeSet = 1;
        } else if (hasPresentMode(VK_PRESENT_MODE_FIFO_KHR)) { //Fallback
            presentMode = VK_PRESENT_MODE_FIFO_KHR;
            presentModeSet = 1;
        }
    }

    //If vsync is disabled or couldn't use a present mode that supports vsync
    if (!presentModeSet && hasPresentMode(VK_PRESENT_MODE_IMMEDIATE_KHR)) {
        presentMode = VK_PRESENT_MODE_IMMEDIATE_KHR;
        presentModeSet = 1;
    }

    if (!presentModeSet) {
        JE_ERROR(err, "Couldn't set a suitable present mode");
        return;
    }

    VkSwapchainKHR oldSwapchain = swapchain;

    VkSwapchainCreateInfoKHR swapchainInfo = {
        VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
        NULL,
        0,
        surface,
        minSwapchainImageCount,
        surfaceFormat.format,
        surfaceFormat.colorSpace,
        extent,
        1,
        imageUsageFlags,
        VK_SHARING_MODE_EXCLUSIVE,
        0,
        NULL,
        transformFlags,
        VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
        presentMode,
        VK_TRUE,
        oldSwapchain //Previous swapchain, in case of recreation
    };

    result = vkCreateSwapchainKHR(device, &swapchainInfo, NULL, &swapchain);
    if (result != VK_SUCCESS) {
        JE_VKERROR(err, "Couldn't create swapchain", result);
        return;
    }

    vkDestroySwapchainKHR(device, oldSwapchain, NULL);

    result = vkGetSwapchainImagesKHR(device, swapchain, &imageCount, NULL);
    if (result != VK_SUCCESS) {
        JE_VKERROR(err, "Couldn't get swapchain image count", result);
        return;
    }

    images = malloc(sizeof(VkImage) * imageCount);
    result = vkGetSwapchainImagesKHR(device, swapchain, &imageCount, images);
    if (result != VK_SUCCESS) {
        JE_VKERROR(err, "Couldn't get swapchain images", result);
        return;
    }

    //Swapchain image creation
    imageViews = malloc(sizeof(VkImageView) * imageCount);

    VkImageSubresourceRange subresourceRange = {
        VK_IMAGE_ASPECT_COLOR_BIT,
        0,
        1,
        0,
        1
    };

    VkImageViewCreateInfo imageViewInfo = {
        VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
        NULL,
        0,
        NULL, //This structure will be reused, and this will be replaced
        VK_IMAGE_VIEW_TYPE_2D,
        surfaceFormat.format,
        {
            VK_COMPONENT_SWIZZLE_IDENTITY,
            VK_COMPONENT_SWIZZLE_IDENTITY,
            VK_COMPONENT_SWIZZLE_IDENTITY,
            VK_COMPONENT_SWIZZLE_IDENTITY,
        },
        subresourceRange
    };

    for (size_t i = 0; i < imageCount; i++) {
        imageViewInfo.image = images[i];

        result = vkCreateImageView(device, &imageViewInfo, NULL,
                                   &imageViews[i]);
        if (result != VK_SUCCESS) {
            JE_VKERROR(err, "Couldn't create image view", result);
            return;
        }
    }

    _jeFramebufferSetSwapchainImageViews(device, imageCount, imageViews);

    _jeCameraUpdateSwapchainViews(imageCount, imageViews, device, err);
}

VkFence _jeSwapchainNextImage(VkDevice device, VkSemaphore ready,
                              uint32_t *index)
{
    vkAcquireNextImageKHR(device, swapchain, UINT64_MAX, ready, VK_NULL_HANDLE,
                          index);

    vkWaitForFences(device, 1, &presentFences[*index], VK_TRUE,
                    UINT64_MAX);
    vkResetFences(device, 1, &presentFences[*index]);

    return presentFences[*index];
}

void _jeSwapchainPresent(VkDevice device, VkQueue queue, VkSemaphore present,
                         uint32_t index)
{
    VkPresentInfoKHR presentInfo = {
        VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
        NULL,
        1,
        &present,
        1,
        &swapchain,
        &index,
        NULL
    };

    vkQueuePresentKHR(queue, &presentInfo);
}

void _jeSwapchainCleanup(VkDevice device, VkInstance instance)
{
    destroySwapchainImageViews(device);
    vkDestroySwapchainKHR(device, swapchain, NULL);

    if (presentFences != 0) {
        for (int i = 0; i < imageCount; i++) {
            vkDestroyFence(device, presentFences[i], NULL);
        }
        free(presentFences);
    }

    if (surface != VK_NULL_HANDLE) {
        vkDestroySurfaceKHR(instance, surface, NULL);
    }

    free(presentModes);
}
