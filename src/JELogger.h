/* Copyright (C) 2017-2018 Sam Bazley and Arnau Bigas
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */


#ifndef JELOGGER_H
#define JELOGGER_H

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#define JE_ERROR(err, format, ...)                                             \
{                                                                              \
    if (err != 0) {                                                            \
        err->file = __FILE__;                                                  \
        err->line = __LINE__;                                                  \
        err->func = __func__;                                                  \
        size_t s = snprintf(NULL, 0, format, ##__VA_ARGS__) + 1;               \
        err->msg = malloc(s);                                                  \
        sprintf(err->msg, format, ##__VA_ARGS__);                              \
    }                                                                          \
}

#define JE_ERROR_INIT(err) \
{\
    err.line = 0;\
}

#define JE_LOG_DEBUG   0x01
#define JE_LOG_WARNING 0x02

typedef struct JEError {
    const char *file;
    int line;
    const char *func;
    char *msg;
} JEError;

/**
 * Checks if the error struct has been set, if there has been an error.
 * @returns true if there is an error, false otherwise.
 */
int jeErrorIsSet(JEError *err);

/**
 * Must be called after the user is done with a JEError object, used to dispose
 * of the dynamically allocated string for the error message.
 */
void jeErrorFree(JEError *err);

/**
 * Sets the flags that determine the output of the logger, disabling or
 * enabling different types of outputs. By default debug and warnings are off.
 * @param flags The flags to be set (JE_LOG_DEBUG and/or JE_LOG_WARNING)
 */
void jeLoggerFlags(uint8_t flags);

/**
 * This function logs debug messages and is only recorded if the logger
 * was initialized with the debug flag enabled. This flag is usually
 * automatically set by the Game_init() function. Disabled by default.
 * @param msg The message to be logged
 */
void jeLoggerDebug(const char *msg, ...);

/**
 * This function logs information messages.
 * @param msg The message to be logged
 */
void jeLoggerInfo(const char *msg, ...);

/**
 * This function logs warning messages (to the standard buffer) and is only
 * recorded if the logger was initialized with the warning flag enabled.
 * This flag is usually automatically set by the Game_init() function.
 * Disabled by default.
 * @param msg The message to be logged
 */
void jeLoggerWarning(const char *msg, ...);

/**
 * This function logs error messages to the ERROR buffer.
 * @param err The error to be logged. Set using the JE_ERROR macro.
 */
void jeLoggerError(JEError *err);

#endif /* JELOGGER_H */
