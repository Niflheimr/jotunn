/* Copyright (C) 2017-2018 Sam Bazley and Arnau Bigas
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef JEVKUTIL_H
#define JEVKUTIL_H

#include <vulkan/vulkan.h>
#include "JELogger.h"

#define JE_VKERROR(err, msg, res) JE_ERROR(err, msg", error %d", res)

/**
 * Used by internal functions to collect statistics about what happens inside
 * a command buffer. If there is a custom draw function it should add the
 * rellevant information to this struct for the statistics to be accurate.
 */
typedef struct {
    unsigned int pipelineBinds;   //< Number of pipeline bind calls
    unsigned int drawCalls;       //< Number of draw calls
    unsigned int drawnObjects;    //< Number of drawn objects
    unsigned int totalPolygons;   //< Number of polygons drawn in total
    unsigned int pushConstsBytes; //< Transfered data through push constants
} JECmdBufferStatistics;

typedef struct {
    size_t textures;   //< Memory used for textures in bytes
    size_t models;     //< Memory used for models in bytes
    size_t buffers;    //< Memory used for general buffers in bytes
} JEMemUsageStatistics;

/**
 * Engine wrapper around the Vulkan command buffer object, including statistics
 */
typedef struct {
    VkCommandBuffer vkHandle;    //< Native vulkan handle
    JECmdBufferStatistics stats; //< Statistics about this command buffer
} JECommandBuffer;

void _jeVkUtilInit(VkQueue queue, VkCommandPool commandPool,
        VkPhysicalDevice device);

VkDeviceMemory jeVkUtilAllocateMemory(VkDevice dev, VkMemoryRequirements memReq,
        VkMemoryPropertyFlagBits memFlags, JEError *err);

void jeVkUtilStageData(VkDevice dev, VkPhysicalDevice physicalDev,
        VkBuffer dstBuffer, size_t size, void *dataSrc, JEError *err);

#endif /* JEVKUTIL_H */
