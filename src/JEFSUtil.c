/* Copyright (C) 2017-2018 Sam Bazley and Arnau Bigas
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include "JEFSUtil.h"

#include <string.h>
#include <stdlib.h>

#ifdef __linux__
#   include <dirent.h>
#   include <sys/stat.h>
#elif _WIN32
#   include <windows.h>
#endif

struct _JEDirectory {
    const char *path;
#ifdef __linux__
    DIR *dir;
#elif _WIN32
    HANDLE hFind;
    WIN32_FIND_DATA findData;
#endif
};

JEDirectory *jeFSUtilOpenDirectory(const char *path)
{
    JEDirectory *ret = malloc(sizeof(JEDirectory));
    ret->path = path;
#ifdef __linux__
    ret->dir = opendir(path);
    if (ret->dir == NULL) {
        free(ret);
        return NULL;
    }
#elif _WIN32
    //ret->hFind = FindFirstFile(path, &ret->findData);
#   error TODO: jeFSUtilOpenDirectory on windows
#endif
    return ret;
}

int jeFSUtilNextFileInDirectory(JEDirectory *dir, char *path, int filter)
{
    int ret = 0;
#ifdef __linux__
    struct dirent *d = 0;
    while(!(ret & filter) && (d = readdir(dir->dir)))  {
        ret = 0;
        strcpy(path, dir->path);
        strcat(path, d->d_name);
        struct stat pathStat;
        stat(path, &pathStat);
        if (S_ISREG(pathStat.st_mode)) ret = JE_FSUTIL_TYPE_REG;
        else ret = JE_FSUTIL_TYPE_DIR;
    }
    ret = ((ret & filter) ? ret : 0);
#elif _WIN32

#   error TODO: jeFSUtilNextFileInDirectory on windows
#endif
    return ret;
}

void jeFSUtilCloseDirectory(JEDirectory *dir)
{
#ifdef __linux__
    closedir(dir->dir);
#elif _WIN32
    //FindClose(dir->hFind);
#   error TODO: jeFSUtilCloseDirectory on windows
#endif
    free(dir);
}

char *jeFSUtilGetFilename(const char *path)
{
    char *dot = strrchr(path, '.');
    char *slash = strrchr(path, '/');

    char *result;
    if (slash == NULL) {
        result = malloc((dot - path + 1) * sizeof(char));
        memcpy(result, path, dot - path + 1);
        result[dot - path] = '\0';
    } else {
        result = malloc((dot - slash) * sizeof(char));
        memcpy(result, slash + 1, dot - slash);
        result[dot - slash - 1] = '\0';
    }
    return result;
}

const char* jeFSUtilGetExtension(const char *path)
{
    const char *dot = strrchr(path, '.');
    if(!dot || dot == path) {
        return "";
    }
    return dot + 1;
}
