/* Copyright (C) 2017-2018 Sam Bazley and Arnau Bigas
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef JEPIPELINE_H
#define JEPIPELINE_H

#include <vulkan/vulkan.h>

#include "JEVkUtil.h"
#include "JELogger.h"

typedef struct _JEPipeline JEPipeline;

void _jePipelineLoad(VkDevice device, JEError *err);

void _jePipelineResizeStaticViewports(VkDevice device, JEError *err);

JEPipeline *jePipelineGet(const char *name);

#ifndef JE_NO_DEFAULT_GRAPHICS
#define JE_DEFAULT_PIPELINE_ARROW 0

JEPipeline *jePipelineGetDefault(int id);
#endif

void jePipelineBind(JEPipeline *pipeline, JECommandBuffer *cmdBuffer);

void jePipelineBindDescriptorSet(JEPipeline *pipeline,
        JECommandBuffer *cmdBuffer, const char *dsName,
        VkDescriptorSet descriptorSet);

int jePipelinePushConstantsIndex(JEPipeline *pipeline, const char *name);

void jePipelinePushConstantsUpdate(JEPipeline *pipeline, JECommandBuffer *cmd,
        int index, const void *data);

void _jePipelineCleanup(VkDevice device);

#endif /* JEPIPELINE_H */
