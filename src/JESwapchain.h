/* Copyright (C) 2017-2018 Sam Bazley and Arnau Bigas
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef JESWAPCHAIN_H
#define JESWAPCHAIN_H

#include "JELogger.h"
#include "JEWindow.h"

#include <vulkan/vulkan.h>

void _jeSwapchainInitSurface(JEWindow window, VkInstance instance,
        JEError *err);

int _jeSwapchainQueueCanPresent(VkPhysicalDevice device, int index);

void _jeSwapchainInit(VkDevice device, VkPhysicalDevice physicalDevice,
        JEError *err);

void _jeSwapchainCreate(int width, int height, VkDevice device,
        VkPhysicalDevice physicalDevice, int vsync, JEError *err);

VkFormat _jeSwapchainFormat();

VkFence _jeSwapchainNextImage(VkDevice device, VkSemaphore ready,
        uint32_t *index);

void _jeSwapchainPresent(VkDevice device, VkQueue queue, VkSemaphore present,
        uint32_t index);

void _jeSwapchainCleanup();

#endif /* JESWAPCHAIN_H */
