/* Copyright (C) 2017-2018 Sam Bazley and Arnau Bigas
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */


#ifndef JEKEYBOARD_H
#define JEKEYBOARD_H

//http://www.usb.org/developers/hidpage/Hut1_12v2.pdf (10 0x07 P53)

typedef enum jeKeyboardKey {
    JE_KEY_NONE           = 0,
    JE_KEY_A              = 1,
    JE_KEY_B              = 2,
    JE_KEY_C              = 3,
    JE_KEY_D              = 4,
    JE_KEY_E              = 5,
    JE_KEY_F              = 6,
    JE_KEY_G              = 7,
    JE_KEY_H              = 8,
    JE_KEY_I              = 9,
    JE_KEY_J              = 10,
    JE_KEY_K              = 11,
    JE_KEY_L              = 12,
    JE_KEY_M              = 13,
    JE_KEY_N              = 14,
    JE_KEY_O              = 15,
    JE_KEY_P              = 16,
    JE_KEY_Q              = 17,
    JE_KEY_R              = 18,
    JE_KEY_S              = 19,
    JE_KEY_T              = 20,
    JE_KEY_U              = 21,
    JE_KEY_V              = 22,
    JE_KEY_W              = 23,
    JE_KEY_X              = 24,
    JE_KEY_Y              = 25,
    JE_KEY_Z              = 26,
    JE_KEY_0              = 27,
    JE_KEY_1              = 28,
    JE_KEY_2              = 29,
    JE_KEY_3              = 30,
    JE_KEY_4              = 31,
    JE_KEY_5              = 32,
    JE_KEY_6              = 33,
    JE_KEY_7              = 34,
    JE_KEY_8              = 35,
    JE_KEY_9              = 36,
    JE_KEY_RETURN         = 37,
    JE_KEY_ESCAPE         = 38,
    JE_KEY_BACKSPACE      = 39,
    JE_KEY_TAB            = 40,
    JE_KEY_SPACE          = 41,
    JE_KEY_MINUS          = 42,
    JE_KEY_EQUALS         = 43,
    JE_KEY_LEFTBRACKET    = 44,
    JE_KEY_RIGHTBRACKET   = 45,
    JE_KEY_BACKSLASH      = 46,
    JE_KEY_NONUSHASH      = 47,
    JE_KEY_SEMICOLON      = 48,
    JE_KEY_APOSTROPHE     = 49,
    JE_KEY_GRAVE          = 50,
    JE_KEY_COMMA          = 51,
    JE_KEY_PERIOD         = 52,
    JE_KEY_SLASH          = 53,
    JE_KEY_CAPSLOCK       = 54,
    JE_KEY_F1             = 55,
    JE_KEY_F2             = 56,
    JE_KEY_F3             = 57,
    JE_KEY_F4             = 58,
    JE_KEY_F5             = 59,
    JE_KEY_F6             = 60,
    JE_KEY_F7             = 61,
    JE_KEY_F8             = 62,
    JE_KEY_F9             = 63,
    JE_KEY_F10            = 64,
    JE_KEY_F11            = 65,
    JE_KEY_F12            = 66,
    JE_KEY_PRINTSCREEN    = 67,
    JE_KEY_SCROLLLOCK     = 68,
    JE_KEY_PAUSE          = 69,
    JE_KEY_INSERT         = 70,
    JE_KEY_HOME           = 71,
    JE_KEY_PGUP           = 72,
    JE_KEY_DELETE         = 73,
    JE_KEY_END            = 74,
    JE_KEY_PGDOWN         = 75,
    JE_KEY_RIGHT          = 76,
    JE_KEY_LEFT           = 77,
    JE_KEY_DOWN           = 78,
    JE_KEY_UP             = 79,
    JE_KEY_NUMLOCK        = 80,
    JE_KEY_NUM_DIVIDE     = 81,
    JE_KEY_NUM_MULTIPLY   = 82,
    JE_KEY_NUM_MINUS      = 83,
    JE_KEY_NUM_PLUS       = 84,
    JE_KEY_NUM_ENTER      = 85,
    JE_KEY_NUM_1          = 86,
    JE_KEY_NUM_2          = 87,
    JE_KEY_NUM_3          = 88,
    JE_KEY_NUM_4          = 89,
    JE_KEY_NUM_5          = 90,
    JE_KEY_NUM_6          = 91,
    JE_KEY_NUM_7          = 92,
    JE_KEY_NUM_8          = 93,
    JE_KEY_NUM_9          = 94,
    JE_KEY_NUM_0          = 95,
    JE_KEY_NUM_PERIOD     = 96,
    JE_KEY_NONUSBACKSLASH = 97,
    JE_KEY_APPLICATION    = 98,

    JE_KEY_LCTRL          = 99,         /* 224 */
    JE_KEY_LSHIFT         = 100,        /* 225 */
    JE_KEY_LALT           = 101,        /* 226 */
    JE_KEY_LGUI           = 102,        /* 227 */
    JE_KEY_RCTRL          = 103,        /* 228 */
    JE_KEY_RSHIFT         = 104,        /* 229 */
    JE_KEY_RALT           = 105,        /* 230 */
    JE_KEY_RGUI           = 106         /* 231 */
} jeKeyboardKey;


void _jeKeyboardSetKeyState(
        jeKeyboardKey sc, jeKeyboardKey ks, char typed [4], int down);

int jeKeyboardIsScancodeDown(jeKeyboardKey sc);

extern const int JE_KEYBOARD_OSTOSTD [];
extern const char *JE_KEYBOARD_KEY_NAMES [];

#endif /* JEKEYBOARD_H */
