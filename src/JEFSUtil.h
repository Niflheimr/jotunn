/* Copyright (C) 2017-2018 Sam Bazley and Arnau Bigas
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef JEFSUTIL_H
#define JEFSUTIL_H

typedef struct _JEDirectory JEDirectory;

#define JE_FSUTIL_TYPE_REG 1
#define JE_FSUTIL_TYPE_DIR 2

/**
 * Opens a directory for iterating over its files later. Has to be closed with
 * jeFSUtilCloseDirectory()
 * @param path The path to the directory to be opened
 * @return A pointer to a directory object
 */
JEDirectory *jeFSUtilOpenDirectory(const char *path);

/**
 * Gets the next file in a directory
 * @param dir The directory in which the files are being searched
 * @param path Pointer to a string, will return the path of the next file
 * @param filter Bitfield of JE_FSUTIL_TYPE flags, function will only return
 * files of this type
 * @return The type of the file, 0 if no file left
 */
int jeFSUtilNextFileInDirectory(JEDirectory *dir, char *path, int filter);

/**
 * Closes a directory previously opened by jeFSUtilOpenDirectory
 * @param dir The directory to be closed
 */
void jeFSUtilCloseDirectory(JEDirectory *dir);

/**
 * Used to get the name of a file from its path
 * @param path The path to the file
 * @return The name of the file, free'd by the user
 */
char *jeFSUtilGetFilename(const char *path);

/**
 * Used to get the extension from its path
 * @param path The path to the file
 * @return The file extension
 */
const char *jeFSUtilGetExtension(const char *path);

#endif /* JEFSUTIL_H */
