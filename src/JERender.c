/* Copyright (C) 2017-2018 Sam Bazley and Arnau Bigas
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include "JERender.h"

#include "JEWindow.h"
#include "JESwapchain.h"
#include "JERenderPass.h"
#include "JEShader.h"
#include "JEPipeline.h"
#include "JECamera.h"
#include "JEModel.h"

#include <string.h>
#include <stdlib.h>

static const char *validationLayers[] = {
    "VK_LAYER_LUNARG_standard_validation"
};

static int numValLayers = 1;

static int checkValidationLayerSupport()
{
    uint32_t layerCount;
    vkEnumerateInstanceLayerProperties(&layerCount, 0);

    VkLayerProperties layers[layerCount];
    vkEnumerateInstanceLayerProperties(&layerCount, layers);

    for (int i = 0; i < numValLayers; i++) {
        int layerFound = 0;

        for(int j = 0; j < layerCount; j++) {
            if (strcmp(validationLayers[i], layers[j].layerName) == 0) {
                layerFound = 1;
                break;
            }
        }

        if (layerFound == 0) {
            return 0;
        }
    }

    return 1;
}

static VkDebugReportCallbackEXT debugObj;

static VkBool32 debugCallback(VkDebugReportFlagsEXT flags,
                              VkDebugReportObjectTypeEXT objectType,
                              uint64_t object, size_t location,
                              int32_t messageCode, const char *pLayerPrefix,
                              const char *pMessage, void *userData)
{
    if (flags & VK_DEBUG_REPORT_DEBUG_BIT_EXT) {
        jeLoggerDebug("Vulkan Debug:\n%s", pMessage);
    } else if (flags & VK_DEBUG_REPORT_ERROR_BIT_EXT) {
        jeLoggerWarning("Potential Vulkan Error:\n%s", pMessage);
    } else if (flags & VK_DEBUG_REPORT_WARNING_BIT_EXT) {
        jeLoggerWarning("Vulkan Warning:\n%s", pMessage);
    } else if(flags & VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT) {
        jeLoggerWarning("Vulkan Performance Warning:\n%s", pMessage);
    }

    return VK_FALSE;
}

static VkInstance instance = VK_NULL_HANDLE;

static VkPhysicalDevice physicalDevice = 0;
static VkDevice device = 0;

static uint32_t presentQueueFamilyIndex = UINT32_MAX;
static uint32_t graphicsQueueFamilyIndex = UINT32_MAX;
static uint32_t transferQueueFamilyIndex = UINT32_MAX;

static VkQueue graphicsQueue;
static VkQueue presentQueue;
static VkQueue transferQueue;

static VkCommandPool graphicsCommandPool;
static VkCommandPool transferCommandPool;

static VkSemaphore imageAvailableSemaphore;
static VkSemaphore presentSemaphore;

void _jeRenderDraw()
{
    //[image ready] --> imageAvailableSempahore
    uint32_t index;
    VkFence renderingFinished = _jeSwapchainNextImage(device,
                                                      imageAvailableSemaphore,
                                                      &index);

    _jeCameraRecordCmdBuffers(index);

    //imageAvailableSemaphore --> [submit] --> presentSemaphore
    _jeCameraSubmitCmdBuffers(device, graphicsQueue, imageAvailableSemaphore,
                              presentSemaphore, renderingFinished, index);

    //TODO: add synchronization to cases in which the graphics queue isn't the
    //same as the presentation queue. Not sure when that happens, though.

    //presentSemaphore --> [image present]
    _jeSwapchainPresent(device, presentQueue, presentSemaphore, index);
}

void _jeRenderResize(int width, int height)
{
    JEError err;
    JE_ERROR_INIT(err);

    //Wait for operations to finish
    vkDeviceWaitIdle(device);

    //TODO: config this
    _jeSwapchainCreate(width, height, device, physicalDevice, 1, &err);
    if (jeErrorIsSet(&err)) {
        jeLoggerError(&err);
    }

    _jePipelineResizeStaticViewports(device, &err);
    if (jeErrorIsSet(&err)) {
        jeLoggerError(&err);
    }

    vkDeviceWaitIdle(device);
}

void _jeRenderInit(const char* appName, uint32_t appVersion, JEError *err)
{
    VkApplicationInfo appInfo = {
        VK_STRUCTURE_TYPE_APPLICATION_INFO,
        NULL,
        appName,
        appVersion,
        "Jotunn",
        VK_MAKE_VERSION(0, 0, 0),
        VK_API_VERSION_1_0
    };

    const char *extensions[] = {
        VK_EXT_DEBUG_REPORT_EXTENSION_NAME,
        VK_KHR_SURFACE_EXTENSION_NAME,
#ifdef _WIN32
        VK_KHR_WIN32_SURFACE_EXTENSION_NAME
#elif __linux__
        VK_KHR_XLIB_SURFACE_EXTENSION_NAME
#endif
    };

    if (!checkValidationLayerSupport()) {
        JE_ERROR(err, "Validation layers enabled but not supported");
        return;
    }

    VkInstanceCreateInfo instanceInfo = {
        VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
        NULL,
        0,
        &appInfo,
        numValLayers,
        validationLayers,
        3,
        extensions
    };

    VkResult result = vkCreateInstance(&instanceInfo, NULL, &instance);
    if (result != VK_SUCCESS) {
        JE_VKERROR(err, "Couldn't create vulkan instance", result);
        return;
    }

    VkDebugReportCallbackCreateInfoEXT debugInfo = {
        VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT,
        NULL,
        VK_DEBUG_REPORT_WARNING_BIT_EXT |
        VK_DEBUG_REPORT_ERROR_BIT_EXT |
        //VK_DEBUG_REPORT_DEBUG_BIT_EXT | Too much info
        VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT,
        debugCallback,
        0
    };

    //TODO: Make it nicer
    result = ((PFN_vkCreateDebugReportCallbackEXT)vkGetInstanceProcAddr(
                instance, "vkCreateDebugReportCallbackEXT"))(instance,
                &debugInfo, NULL, &debugObj);
    if (result != VK_SUCCESS) {
        JE_VKERROR(err, "Couldn't create a Vulkan debugging callback", result);
        return;
    }

    //Acquire rendering surface

    _jeSwapchainInitSurface(_jeWindowHandle(), instance, err);
    if(jeErrorIsSet(err)) {
        return;
    }

    //Get physical devices

    int requiredExtensionsCounts = 1;
    const char *requiredExtensions[] = {VK_KHR_SWAPCHAIN_EXTENSION_NAME};

    uint32_t deviceCount = 0;
    result = vkEnumeratePhysicalDevices(instance, &deviceCount, NULL);
    if (result != VK_SUCCESS) {
        JE_VKERROR(err, "Failed to get vulkan physical devices", result);
        return;
    }

    if (deviceCount == 0) {
        JE_ERROR(err, "Couldn't detect any vulkan capable devices");
        return;
    }

    VkPhysicalDevice devices[deviceCount];
    result = vkEnumeratePhysicalDevices(instance, &deviceCount, devices);
    if (result != VK_SUCCESS) {
        JE_VKERROR(err, "Failed to get vulkan physical devices", result);
        return;
    }

    for (uint32_t i = 0; i < deviceCount; i++) {
        VkPhysicalDevice currentDevice = devices[i];

        uint32_t extensionCount;
        result = vkEnumerateDeviceExtensionProperties(currentDevice, NULL,
                                                      &extensionCount, NULL);
        if (result != VK_SUCCESS) {
            JE_VKERROR(err, "Couldn't count the device's extensions",
                       result);
            return;
        }

        VkExtensionProperties properties[extensionCount];
        result = vkEnumerateDeviceExtensionProperties(currentDevice, NULL,
                                                      &extensionCount,
                                                      properties);
        if (result != VK_SUCCESS) {
            JE_VKERROR(err, "Couldn't enumerate the device's extensions",
                       result);
            return;
        }

        int shouldSkip = 0;
        for (int j = 0; j < requiredExtensionsCounts; j++) {
            const char *extension = requiredExtensions[j];
            shouldSkip = 1;
            for (int k = 0; k < extensionCount; k++) {
                if (strcmp(properties[k].extensionName, extension) == 0) {
                    shouldSkip = 0;
                    break;
                }
            }
            if (shouldSkip) {
                break;
            }
        }

        if (shouldSkip) {
            continue;
        }

        uint32_t queueFamilyCount = 0;
        vkGetPhysicalDeviceQueueFamilyProperties(currentDevice,
                                                 &queueFamilyCount, NULL);

        VkQueueFamilyProperties familyProperties[queueFamilyCount];
        vkGetPhysicalDeviceQueueFamilyProperties(currentDevice,
                                                 &queueFamilyCount,
                                                 familyProperties);

        int canUseGraphicsQueue = 0;

        for (uint32_t j = 0; j < queueFamilyCount; j++) {
            int supportsPresentation =
                _jeSwapchainQueueCanPresent(currentDevice, j);

            if (presentQueueFamilyIndex == UINT32_MAX && supportsPresentation) {
                presentQueueFamilyIndex = j;
            }

            if (familyProperties[j].queueCount > 0 &&
                familyProperties[j].queueFlags & VK_QUEUE_GRAPHICS_BIT &&
                graphicsQueueFamilyIndex == UINT32_MAX) {
                graphicsQueueFamilyIndex = j;
                canUseGraphicsQueue = familyProperties[j].queueFlags &
                    VK_QUEUE_GRAPHICS_BIT;
            }

            //Use a different queue family for transfer if possible
            if (familyProperties[j].queueFlags & VK_QUEUE_TRANSFER_BIT &&
                j != graphicsQueueFamilyIndex &&
                transferQueueFamilyIndex == UINT32_MAX) {
                transferQueueFamilyIndex = j;
            }
        }

        //Couldn't use a different family, so use the same queue as graphics
        if (transferQueueFamilyIndex == UINT32_MAX && canUseGraphicsQueue) {
            transferQueueFamilyIndex = graphicsQueueFamilyIndex;
        }

        if (graphicsQueueFamilyIndex != UINT32_MAX &&
            presentQueueFamilyIndex != UINT32_MAX &&
            transferQueueFamilyIndex != UINT32_MAX) {
            physicalDevice = currentDevice;
            break;
        }
    }

    if (physicalDevice == 0) {
        JE_ERROR(err, "No compatible vulkan device found!");
        return;
    }

    //Create logical device

    int queues = 1;
    VkDeviceQueueCreateInfo queueCreationInfos[3];
    float queuePriorities[] = {1.0f};

    queueCreationInfos[0].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    queueCreationInfos[0].pNext = NULL;
    queueCreationInfos[0].flags = 0;
    queueCreationInfos[0].queueFamilyIndex = graphicsQueueFamilyIndex;
    queueCreationInfos[0].queueCount = 1;
    queueCreationInfos[0].pQueuePriorities = queuePriorities;

    if (graphicsQueueFamilyIndex != presentQueueFamilyIndex) {
        queueCreationInfos[1].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        queueCreationInfos[1].pNext = NULL;
        queueCreationInfos[1].flags = 0;
        queueCreationInfos[1].queueFamilyIndex = presentQueueFamilyIndex;
        queueCreationInfos[1].queueCount = 1;
        queueCreationInfos[1].pQueuePriorities = queuePriorities;
        queues++;
    }
    if (graphicsQueueFamilyIndex != transferQueueFamilyIndex) {
        queueCreationInfos[queues].sType =
            VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        queueCreationInfos[queues].pNext = NULL;
        queueCreationInfos[queues].flags = 0;
        queueCreationInfos[queues].queueFamilyIndex = transferQueueFamilyIndex;
        queueCreationInfos[queues].queueCount = 1;
        queueCreationInfos[queues].pQueuePriorities = queuePriorities;
        queues++;
    }

    VkDeviceCreateInfo deviceInfo = {
        VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
        NULL,
        0,
        queues,
        queueCreationInfos,
        0,
        NULL,
        requiredExtensionsCounts,
        requiredExtensions,
        NULL
    };

    result = vkCreateDevice(physicalDevice, &deviceInfo, NULL, &device);
    if (result != VK_SUCCESS) {
        JE_VKERROR(err, "Failed to create vulkan logical device", result);
        return;
    }

    //Init swapchain

    _jeSwapchainInit(device, physicalDevice, err);
    if (jeErrorIsSet(err)) {
        return;
    }

    //Create Queues

    vkGetDeviceQueue(device, graphicsQueueFamilyIndex, 0, &graphicsQueue);
    vkGetDeviceQueue(device, presentQueueFamilyIndex, 0, &presentQueue);
    vkGetDeviceQueue(device, transferQueueFamilyIndex, 0, &transferQueue);

    //Create Semaphores

    VkSemaphoreCreateInfo semaphoreInfo = {
        VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
        NULL,
        0
    };

    result = vkCreateSemaphore(device, &semaphoreInfo, NULL,
                               &imageAvailableSemaphore);
    if (result != VK_SUCCESS) {
        JE_VKERROR(err, "Failed to create image available semaphore", result);
        return;
    }

    result = vkCreateSemaphore(device, &semaphoreInfo, NULL, &presentSemaphore);
    if (result != VK_SUCCESS) {
        JE_VKERROR(err, "Failed to create present semaphore", result);
        return;
    }

    //Create command pools

    VkCommandPoolCreateInfo graphicsCmdPoolInfo = {
        VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
        NULL,
        VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
        graphicsQueueFamilyIndex
    };

    result = vkCreateCommandPool(device, &graphicsCmdPoolInfo, NULL,
                                 &graphicsCommandPool);
    if (result != VK_SUCCESS) {
        JE_VKERROR(err, "Failed to create graphics command pool", result);
        return;
    }

    VkCommandPoolCreateInfo transferCmdPoolInfo = {
        VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
        NULL,
        0,
        transferQueueFamilyIndex
    };

    result = vkCreateCommandPool(device, &transferCmdPoolInfo, NULL,
                                 &transferCommandPool);
    if (result != VK_SUCCESS) {
        JE_VKERROR(err, "Failed to create transfer command pool", result);
        return;
    }

    _jeVkUtilInit(transferQueue, transferCommandPool, physicalDevice);

    //Create swapchain

    //TODO enable vsync through config
    _jeSwapchainCreate(jeWindowGetWidth(), jeWindowGetHeight(), device,
                       physicalDevice, 1, err);
    if (jeErrorIsSet(err)) {
        return;
    }

    //Load Render Techniques, Shaders, Pipelines, Camera and Model Systems

    _jeRenderPassLoad(device, err);
    if(jeErrorIsSet(err)) {
        return;
    }

    _jeShaderLoad(device, err);
    if(jeErrorIsSet(err)) {
        return;
    }

    _jePipelineLoad(device, err);
    if(jeErrorIsSet(err)) {
        return;
    }

    _jeCameraInit(device, physicalDevice, graphicsCommandPool, err);
    if(jeErrorIsSet(err)) {
        return;
    }

    _jeModelLoad(device, physicalDevice, err);
    if(jeErrorIsSet(err)) {
        return;
    }
}

void _jeRenderCleanup()
{
    if (device != VK_NULL_HANDLE) {
        vkDeviceWaitIdle(device);

        if (graphicsCommandPool != VK_NULL_HANDLE) {
            vkDestroyCommandPool(device, graphicsCommandPool, NULL);
            graphicsCommandPool = VK_NULL_HANDLE;
        }

        if (transferCommandPool != VK_NULL_HANDLE) {
            vkDestroyCommandPool(device, transferCommandPool, NULL);
            transferCommandPool = VK_NULL_HANDLE;
        }

        if (imageAvailableSemaphore != VK_NULL_HANDLE) {
            vkDestroySemaphore(device, imageAvailableSemaphore, NULL);
        }

        if (presentSemaphore != VK_NULL_HANDLE) {
            vkDestroySemaphore(device, presentSemaphore, NULL);
        }

        _jeModelCleanup(device);

        _jePipelineCleanup(device);

        _jeShaderCleanup(device);

        _jeRenderPassCleanup(device);

        _jeCameraCleanup(device);

        _jeSwapchainCleanup(device, instance);

        vkDestroyDevice(device, NULL);
    }
}

void _jeRenderCleanupFinal()
{
    ((PFN_vkDestroyDebugReportCallbackEXT)vkGetInstanceProcAddr(instance,
        "vkDestroyDebugReportCallbackEXT"))(instance, debugObj, 0);
    if (instance != VK_NULL_HANDLE) {
        vkDestroyInstance(instance, NULL);
    }
}
