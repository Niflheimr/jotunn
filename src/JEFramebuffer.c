/* Copyright (C) 2017-2018 Sam Bazley and Arnau Bigas
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include "JEFramebuffer.h"

#include <string.h>
#include <ujson.h>

#include "JERenderPass.h"

struct _JEFramebufferAttachment {
    char* name;
    VkImage image;
    VkImageView imageView;
    VkDeviceMemory memory;
    VkFormat format;
};

static int swapchainViewCount;
static VkImageView* swapchainViews;
static VkDevice device;

void _jeFramebufferSetSwapchainImageViews(VkDevice dev, int count,
                                          VkImageView* views)
{
    device = dev;
    swapchainViewCount = count;
    swapchainViews = views;
}

#define ERRCHK() if (jeErrorIsSet((JEError *) err)) return;

static void parseAttachment(JsonObject* json, int width, int height,
                            JEFramebufferAttachment* attachment, JEError *err)
{
    VkResult result;
    JsonError *jerr = (JsonError *) err;

    VkExtent3D extent = {width, height, 1};

    VkFormat format;
    VkImageLayout layout;
    VkImageUsageFlagBits usage;

    jsonGetInt(json, "format", (int*) &format, jerr); ERRCHK();
    jsonGetInt(json, "layout", (int*) &layout, jerr); ERRCHK();
    jsonGetInt(json, "usage",  (int*) &usage,  jerr); ERRCHK();

    VkImageCreateInfo imageInfo = {
        VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
        NULL,
        0,
        VK_IMAGE_TYPE_2D,
        format,
        extent,
        1,
        1,
        VK_SAMPLE_COUNT_1_BIT,
        VK_IMAGE_TILING_OPTIMAL,
        usage,
        VK_SHARING_MODE_EXCLUSIVE,
        0,
        NULL,
        VK_IMAGE_LAYOUT_UNDEFINED//layout
    };

    result = vkCreateImage(device, &imageInfo, NULL, &attachment->image);
    if (result != VK_SUCCESS) {
        JE_VKERROR(err, "Couldn't create framebuffer image", result);
        return;
    }

    VkMemoryRequirements memReqs;
    vkGetImageMemoryRequirements(device, attachment->image, &memReqs);

    attachment->memory = jeVkUtilAllocateMemory(device, memReqs,
                                            VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                                            err);
    if (jeErrorIsSet(err)) return;

    vkBindImageMemory(device, attachment->image, attachment->memory, 0);

    VkImageViewCreateInfo viewInfo = {
        VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
        NULL,
        0,
        attachment->image,
        VK_IMAGE_VIEW_TYPE_2D,
        format,
        {
            VK_COMPONENT_SWIZZLE_IDENTITY,
            VK_COMPONENT_SWIZZLE_IDENTITY,
            VK_COMPONENT_SWIZZLE_IDENTITY,
            VK_COMPONENT_SWIZZLE_IDENTITY,
        },
        {
            VK_IMAGE_ASPECT_DEPTH_BIT,
            0, 1, 0, 1
        }
    };

    result = vkCreateImageView(device, &viewInfo, NULL,
                               &attachment->imageView);
    if (result != VK_SUCCESS) {
        JE_VKERROR(err, "Couldn't create depth image view", result);
    }
}

#undef ERRCHK

JEFramebuffer *_jeFramebufferCreate(const char *layout, int width, int height,
                                    JEError *err)
{
    JsonError *jerr = (JsonError *) err;
    char file[256] = "res/framebuffer/";
    strcat(file, layout);
    strcat(file, ".json");

    JsonObject *json = jsonParseFile(file, jerr);
    if (jeErrorIsSet(err)) return NULL;

    JEFramebuffer *framebuffer = malloc(sizeof(JEFramebuffer));

    JsonArray *attachments;
    jsonGetArray(json, "attachments", &attachments, jerr);
    if (jeErrorIsSet(err)) return NULL;

    framebuffer->attachmentCount = attachments->used;
    framebuffer->attachments     =
        malloc(sizeof(JEFramebufferAttachment) * attachments->used);
    VkImageView views[attachments->used];

    for (int i = 0; i < attachments->used; i++) {
        parseAttachment(attachments->vals[i], width, height,
                        &framebuffer->attachments[i], err);
        if (jeErrorIsSet(err)) return NULL;
        views[i] = framebuffer->attachments[i].imageView;
    }

    char *rpname;
    jsonGetString(json, "renderPass", &rpname, jerr);
    if (jeErrorIsSet(err)) return NULL;

    JERenderPass *rp = jeRenderPassGet(rpname);

    VkFramebufferCreateInfo framebufferInfo = {
        VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
        NULL,
        0,
        jeRenderPassVk(rp),
        attachments->used,
        views,
        width,
        height,
        1
    };

    VkResult result = vkCreateFramebuffer(device, &framebufferInfo, NULL,
                                          &framebuffer->vk);
    if (result != VK_SUCCESS) {
        JE_VKERROR(err, "Couldn't create framebuffer", result);
        return NULL;
    }

    jsonCleanup(json);

    framebuffer->width  = width;
    framebuffer->height = height;

    return framebuffer;
}

JEFramebuffer *_jeFramebufferCreatesc(const char* layout, int width, int height,
                                      JEError *err)
{
    JsonError *jerr = (JsonError *) err;
    char file[256] = "res/framebuffer/";
    strcat(file, layout);
    strcat(file, ".json");

    JsonObject *json = jsonParseFile(file, jerr);
    if (jeErrorIsSet(err)) return NULL;

    JEFramebuffer *framebuffers = malloc(sizeof(JEFramebuffer) *
                                         swapchainViewCount);

    JsonArray *attachments;
    jsonGetArray(json, "attachments", &attachments, jerr);
    if (jeErrorIsSet(err)) return NULL;

    VkImageView views[attachments->used];
    JEFramebufferAttachment *fbattachments =
        malloc(sizeof(JEFramebufferAttachment) * (attachments->used));

    //First attachment is ALWAYS the swapchain image
    for (int i = 1; i < attachments->used; i++) {
        parseAttachment(attachments->vals[i], width, height,
                        &fbattachments[i], err);
        if (jeErrorIsSet(err)) return NULL;
        views[i] = fbattachments[i].imageView;
    }

    char *rpname;
    jsonGetString(json, "renderPass", &rpname, jerr);
    if (jeErrorIsSet(err)) return NULL;

    JERenderPass *rp = jeRenderPassGet(rpname);
    VkRenderPass renderpass = jeRenderPassVk(rp);

    VkFramebufferCreateInfo framebufferInfo = {
        VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
        NULL,
        0,
        renderpass,
        attachments->used,
        views,
        width,
        height,
        1
    };

    for (int i = 0; i < swapchainViewCount; i++) {
        views[0] = swapchainViews[i];
        framebuffers[i].attachments     = fbattachments;
        framebuffers[i].attachmentCount = attachments->used;

        framebuffers[i].width  = width;
        framebuffers[i].height = height;

        VkResult result = vkCreateFramebuffer(device, &framebufferInfo, NULL,
                                              &framebuffers[i].vk);
        if (result != VK_SUCCESS) {
            JE_VKERROR(err, "Couldn't create framebuffer", result);
            return NULL;
        }
    }

    jsonCleanup(json);

    return framebuffers;
}

static void destroyFramebufferAttachment(JEFramebufferAttachment* attachment)
{
    vkDestroyImageView(device, attachment->imageView, NULL);
    vkDestroyImage(device, attachment->image, NULL);
    vkFreeMemory(device, attachment->memory, NULL);
}

void _jeFramebufferCleanup(JEFramebuffer* framebuffer)
{
    for (int i = 0; i < framebuffer->attachmentCount; i++) {
        destroyFramebufferAttachment(&framebuffer->attachments[i]);
    }
    vkDestroyFramebuffer(device, framebuffer->vk, NULL);

    free(framebuffer->attachments);
    free(framebuffer);
}

void _jeFramebufferCleanupsc(JEFramebuffer* framebuffers)
{
    //The first attachment will ALWAYS be the swapchain image
    for (int i = 1; i < framebuffers[0].attachmentCount; i++) {
        destroyFramebufferAttachment(&framebuffers[0].attachments[i]);
    }

    for (int i = 0; i < swapchainViewCount; i++) {
        vkDestroyFramebuffer(device, framebuffers[i].vk, NULL);
    }

    free(framebuffers[0].attachments);
    free(framebuffers);
}
