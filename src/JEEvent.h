/* Copyright (C) 2017-2018 Sam Bazley and Arnau Bigas
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef JEEVENT_H
#define JEEVENT_H

#include "JEKeyboard.h"
#include <stdint.h>

typedef enum JEEventType {
    JE_EVENT_KEYBOARD,
    JE_EVENT_WINDOW_ENTER,
    JE_EVENT_WINDOW_LEAVE,
    JE_EVENT_MOUSE_BUTTON,
    JE_EVENT_MOUSE_MOVE
} JEEventType;

typedef struct JEEventKeyboard {
    uint32_t type;
    char typed [4];
    jeKeyboardKey scancode;
    jeKeyboardKey keysym;
    uint8_t down;
} JEEventKeyboard;

typedef struct JEEventWindowEnter {
    uint32_t type;
} JEEventWindowEnter;

typedef struct JEEventWindowLeave {
    uint32_t type;
} JEEventWindowLeave;

typedef struct JEEventMouseButton {
    uint32_t type;
    uint8_t button;
    uint8_t down;
    uint16_t x, y;
} JEEventMouseButton;

typedef struct JEEventMouseMove {
    uint32_t type;
    int16_t dx, dy;
    uint16_t x, y;
} JEEventMouseMove;

typedef union JEEvent {
    uint32_t type;
    JEEventKeyboard keyboard;
    JEEventWindowEnter windowEnter;
    JEEventWindowLeave windowLeave;
    JEEventMouseButton mouseButton;
    JEEventMouseMove mouseMove;
} JEEvent;

void _jeEventSubmit(JEEvent *ev);
int jeEventPoll(JEEvent *ev);

#endif /* JEEVENT_H */
