/* Copyright (C) 2017-2018 Sam Bazley and Arnau Bigas
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include "JEEvent.h"
#include <stdlib.h>

typedef struct JEEventEntry {
    JEEvent ev;
    struct JEEventEntry *next;
} JEEventEntry;

static JEEventEntry *firstEnt = 0, *currentEnt;

void _jeEventSubmit(JEEvent *ev)
{
    JEEventEntry *new = malloc(sizeof(JEEventEntry));
    if (firstEnt) { //at least one event already exists
        currentEnt->next = new;
        currentEnt = new;
    } else {
        currentEnt = firstEnt = new;
    }

    currentEnt->ev = *ev;
    currentEnt->next = 0;
}

int jeEventPoll(JEEvent *ev)
{
    if (!firstEnt) return 0; //no events

    static JEEventEntry *pollEnt = 0;

    if (pollEnt == 0)
        pollEnt = firstEnt;

    *ev = pollEnt->ev;

    JEEventEntry *next = pollEnt->next;
    free(pollEnt);
    pollEnt = next;

    if (!pollEnt)
        firstEnt = 0;

    return 1;
}
