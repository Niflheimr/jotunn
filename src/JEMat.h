/* Copyright (C) 2017-2018 Sam Bazley and Arnau Bigas
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef JEMAT_H
#define JEMAT_H

#include <math.h>
#include "JEVec.h"

typedef float jeMat4f[4][4];
typedef float jeMat3f[3][3];

static inline void jeMat4One(jeMat4f *mat)
{
#define A(x,y) (*mat)[x][y] = 1.0f;
    A(0,0); A(0,1); A(0,2); A(0,3);
    A(1,0); A(1,1); A(1,2); A(1,3);
    A(2,0); A(2,1); A(2,2); A(2,3);
    A(3,0); A(3,1); A(3,2); A(3,3);
#undef A
}

static inline void jeMat4Perspective(float fov, float aspect, float zNear,
        float zFar, jeMat4f *result)
{
    float t = tan(fov / 2.0f);

#define RESULT(x, y) (*result)[x][y]
    RESULT(0, 0) = 1.0f / (aspect * t);
    RESULT(0, 1) = 0.0f;
    RESULT(0, 2) = 0.0f;
    RESULT(0, 3) = 0.0f;
    RESULT(1, 0) = 0.0f;
    RESULT(1, 1) = 1.0f / t;
    RESULT(1, 2) = 0.0f;
    RESULT(1, 3) = 0.0f;
    RESULT(2, 0) = 0.0f;
    RESULT(2, 1) = 0.0f;
    RESULT(2, 2) = zFar / (zNear - zFar);
    RESULT(2, 3) = -1.0f;
    RESULT(3, 0) = 0.0f;
    RESULT(3, 1) = 0.0f;
    RESULT(3, 2) = -(zFar * zNear) / (zFar - zNear);
    RESULT(3, 3) = 0.0f;
#undef RESULT
}

static inline void jeMat4LookAt(jeVec3f pos, jeVec3f center, jeVec3f up,
        jeMat4f *result)
{
    jeVec3f f = jeVec3Normalize(jeVec3Sub(center, pos));
    jeVec3f s = jeVec3Normalize(jeVec3Cross(f, up));
    jeVec3f u = jeVec3Cross(s, f);
#define RESULT(x, y) (*result)[x][y]
    RESULT(0, 0) =  s.x;
    RESULT(1, 0) =  s.y;
    RESULT(2, 0) =  s.z;
    RESULT(3, 0) = -jeVec3Dot(s, pos);
    RESULT(0, 1) =  u.x;
    RESULT(1, 1) =  u.y;
    RESULT(2, 1) =  u.z;
    RESULT(3, 1) = -jeVec3Dot(u, pos);
    RESULT(0, 2) = -f.x;
    RESULT(1, 2) = -f.y;
    RESULT(2, 2) = -f.z;
    RESULT(3, 2) =  jeVec3Dot(f, pos);
    RESULT(0, 3) = 0.0f;
    RESULT(1, 3) = 0.0f;
    RESULT(2, 3) = 0.0f;
    RESULT(3, 3) = 1.0f;
#undef RESULT
}

static inline void jeMat4Mul(const jeMat4f *a, const jeMat4f *b,
        jeMat4f *result)
{
#define A (*a)
#define B (*b)
#define AB(x,y) (*result)[x][y] = A[0][y] * B[x][0] + A[1][y] * B[x][1]\
                                + A[2][y] * B[x][2] + A[3][y] * B[x][3]
    AB(0,0); AB(0,1); AB(0,2); AB(0,3);
    AB(1,0); AB(1,1); AB(1,2); AB(1,3);
    AB(2,0); AB(2,1); AB(2,2); AB(2,3);
    AB(3,0); AB(3,1); AB(3,2); AB(3,3);
#undef A
#undef B
#undef AB
}

#endif /* JEMAT_H */
