/* Copyright (C) 2017-2018 Sam Bazley and Arnau Bigas
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef JECAMERA_H
#define JECAMERA_H

#include <vulkan/vulkan.h>

#include "JEFramebuffer.h"
#include "JELogger.h"
#include "JEVkUtil.h"
#include "JEMat.h"

#define JE_CAMERA_FLAG_SCREEN 1
#define JE_CAMERA_FLAG_RECORD_ONCE 2

typedef void (*JECameraCallback)(JECommandBuffer *commandBuffer,
        JEFramebuffer *framebuffer);

typedef struct _JECamera JECamera;

typedef enum {
    JE_CAMERA_PROJECTION_ORTHOGRAPHIC,
    JE_CAMERA_PROJECTION_PERSPECTIVE
} JECameraProjection;

typedef struct {
    const char *fbLayout;
    int flags;
    JECameraProjection proj;
    JECameraCallback callback;
    float fov;
    int width;
    int height;
    float near;
    float far;
} JECameraInfo;

void _jeCameraInit(VkDevice device, VkPhysicalDevice physicalDevice,
        VkCommandPool cmdPool, JEError *err);

void _jeCameraUpdateSwapchainViews(int count, VkImageView *imageViews,
        VkDevice device, JEError *err);

JECamera* jeCameraAdd(JECameraInfo info, JEError *err);

void jeCameraSetActive(JECamera *camera, int active);

void jeCameraSetPosition(JECamera *camera, jeVec3f position);

void jeCameraLookAt(JECamera *camera, jeVec3f position);

VkDescriptorSet jeCameraDescriptorSet(JECamera *camera);

void _jeCameraRecordCmdBuffers(int imageIndex);

void _jeCameraSubmitCmdBuffers(VkDevice device, VkQueue queue, VkSemaphore wait,
        VkSemaphore finish, VkFence presentFence, int imageIndex);

void _jeCameraCleanup(VkDevice device);

#endif /* JECAMERA_H */
