/* Copyright (C) 2017-2018 Sam Bazley and Arnau Bigas
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include "JETimer.h"

#define JE_TIMER_SRC CLOCK_MONOTONIC_RAW

#define DIFF(a, b) (long) (((b.tv_sec - a.tv_sec) * 1000000000)\
                           + (b.tv_nsec - a.tv_nsec))

void jeTimerStart(JETimer *timer)
{
#ifdef __linux__
    clock_gettime(JE_TIMER_SRC, &timer->start);
#elif _WIN32

#endif
}

int jeTimerElapsedSeconds(JETimer *timer)
{
#ifdef __linux__
    struct timespec end;
    clock_gettime(JE_TIMER_SRC, &end);
    return end.tv_sec - timer->start.tv_sec;
#elif _WIN32

#endif
}

int jeTimerElapsedMilliseconds(JETimer *timer)
{
#ifdef __linux__
    struct timespec end;
    clock_gettime(JE_TIMER_SRC, &end);
    return DIFF(timer->start, end) / 1000000;
#elif _WIN32

#endif
}

long jeTimerElapsedMicroseconds(JETimer *timer)
{
#ifdef __linux__
    struct timespec end;
    clock_gettime(JE_TIMER_SRC, &end);
    return DIFF(timer->start, end) / 1000;
#elif _WIN32

#endif
}
