/* Copyright (C) 2017-2018 Sam Bazley and Arnau Bigas
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include "JEShader.h"

#include <string.h>
#include <stdlib.h>

#include "JEFSUtil.h"

#define JE_VKERROR(err, msg, res) JE_ERROR(err, msg", error %d", res)

static JEShader *shaders = 0;
static int numShaders;

#ifndef JE_NO_DEFAULT_GRAPHICS
#   define JE_DEFAULT_SHADER_COUNT 2
extern const char _binary_arrow_vert_start;
extern const char _binary_arrow_vert_end;

extern const char _binary_arrow_frag_start;
extern const char _binary_arrow_frag_end;

static JEShader defaultShaders[JE_DEFAULT_SHADER_COUNT];
#endif

static VkShaderStageFlagBits getStage(const char *filename)
{
    const char *dot = strrchr(filename, '.');
    if (!dot || dot == filename) {
        return 0;
    }

    const char *stage = dot + 1;
    if (strcmp(stage, "frag") == 0) {
        return VK_SHADER_STAGE_FRAGMENT_BIT;
    } else if (strcmp(stage, "vert") == 0) {
        return VK_SHADER_STAGE_VERTEX_BIT;
    } else {
        return 0;
    }
}

static JEShader loadShader(const char *file, VkDevice device, JEError *err)
{
    JEShader shader;

    shader.name = jeFSUtilGetFilename(file);
    shader.stage = getStage(shader.name);
    if (shader.stage == 0) {
        JE_ERROR(err, "Could not determine the stage of shader file %s, does it"
                 " have the standard name of <name>.<stage>.spv?", shader.name);
        return shader;
    }

    FILE *f = fopen(file, "rb");
    if (f == 0) {
        JE_ERROR(err, "Couldn't open shader file %s", file);
        return shader;
    }

    fseek(f, 0, SEEK_END);
    size_t size = ftell(f);
    rewind(f);

    uint32_t *code = malloc(sizeof(uint32_t) * (size/4));
    fread(code, sizeof(uint32_t), size/4, f);

    fclose(f);

    VkShaderModuleCreateInfo info = {
        VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
        0,
        0,
        size,
        code
    };

    VkResult res = vkCreateShaderModule(device, &info, 0, &shader.module);
    if (res != VK_SUCCESS) {
        JE_VKERROR(err, "Couldn't create shader module", res);
    }

    free(code);

    return shader;
}

#ifndef JE_NO_DEFAULT_GRAPHICS
static JEShader loadDefaultShader(const char *name, VkDevice device,
                                  VkShaderStageFlagBits stage, const char *start,
                                  const char *end, JEError *err)
{
    JEShader shader;

    shader.name = (char*) name;
    shader.stage = stage;

    VkShaderModuleCreateInfo info = {
        VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
        0,
        0,
        end-start,
        (uint32_t*) start
    };

    VkResult res = vkCreateShaderModule(device, &info, 0, &shader.module);
    if (res != VK_SUCCESS) {
        JE_VKERROR(err, "Couldn't create default shader", res);
    }

    return shader;
}
#endif

void _jeShaderLoad(VkDevice device, JEError *err)
{
    JEDirectory *dir = jeFSUtilOpenDirectory("res/shader/");
    if (dir != NULL) {
        shaders = calloc(0, sizeof(JEShader));
        char file[256];
        while (jeFSUtilNextFileInDirectory(dir, file, JE_FSUTIL_TYPE_REG)) {
            const char *ext = jeFSUtilGetExtension(file);
            if (ext == 0 || strcmp("spv", ext) != 0) {
                continue;
            }

            JEShader shader = loadShader(file, device, err);

            if (jeErrorIsSet(err)) {
                return;
            } else {
                shaders = realloc(shaders, ++numShaders * sizeof(JEShader));
                shaders[numShaders - 1] = shader;
            }
        }
        jeFSUtilCloseDirectory(dir);
    }

#ifndef JE_NO_DEFAULT_GRAPHICS

#   define LOAD_SHADER(id, name, stage, vkStage) defaultShaders[id] =\
        loadDefaultShader(#name"."#stage, device, vkStage,\
                          &_binary_##name##_##stage##_start,\
                          &_binary_##name##_##stage##_end, err)

#   define LOAD_VERT(id, name) LOAD_SHADER(id, name, vert,\
                                           VK_SHADER_STAGE_VERTEX_BIT)
#   define LOAD_FRAG(id, name) LOAD_SHADER(id, name, frag,\
                                           VK_SHADER_STAGE_FRAGMENT_BIT)

    LOAD_VERT(JE_DEFAULT_SHADER_ARROW_VERT, arrow);
    LOAD_FRAG(JE_DEFAULT_SHADER_ARROW_FRAG, arrow);
#endif
}

#ifndef JE_NO_DEFAULT_GRAPHICS
JEShader *jeShaderGetDefault(int id)
{
    return &defaultShaders[id];
}
#endif

JEShader *jeShaderGet(const char *name)
{
    if (shaders != 0) {
        for (int i = 0; i < numShaders; i++) {
            JEShader *shader = &shaders[i];
            if (strcmp(shader->name, name) == 0) {
                return shader;
            }
        }
    }
    return 0;
}

void _jeShaderCleanup(VkDevice device)
{
    for (int i = 0; i < numShaders; i++) {
        vkDestroyShaderModule(device, shaders[i].module, 0);
        free(shaders[i].name);
    }
    free(shaders);
    numShaders = 0;
#ifndef JE_NO_DEFAULT_GRAPHICS
    for (int i = 0; i < JE_DEFAULT_SHADER_COUNT; i++) {
        vkDestroyShaderModule(device, defaultShaders[i].module, 0);
    }
#endif
}
