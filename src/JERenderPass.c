/* Copyright (C) 2017-2018 Sam Bazley and Arnau Bigas
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include "JERenderPass.h"

#include <string.h>
#include <stdlib.h>

#include "ujson.h"
#include "JEFSUtil.h"
#include "JESwapchain.h"

struct _JERenderPass {
    char *name;
    VkRenderPass vk;
};

static int numRenderPasses;
static JERenderPass *renderPasses;

#ifndef JE_NO_DEFAULT_GRAPHICS
#   define JE_DEFAULT_RENDERPASS_COUNT 1
extern const char _binary_forward_json_start;

static JERenderPass defaultRenderPasses[JE_DEFAULT_RENDERPASS_COUNT];
static const char* defaultSources[] = {
    &_binary_forward_json_start
};
static const char* defaultNames[] = {
    "forward"
};

JERenderPass *jeRenderPassGetDefault(int id)
{
    return &defaultRenderPasses[id];
}
#endif

JERenderPass *jeRenderPassGet(const char* name)
{
    for (int i = 0; i < numRenderPasses; i++) {
        if(strcmp(name, renderPasses[i].name) == 0) {
            return &renderPasses[i];
        }
    }
    return 0;
}

//Used by the json parsing functions
#define ERRCHK()  if(jeErrorIsSet((JEError *) err)) return;
#define ERRCHKZ() if(jeErrorIsSet((JEError *) err)) return 0;

//This is needed to parse ints as enums
#define E(X) (int*) &X

static VkAttachmentDescription *parseAttachments(JsonArray *attachments,
                                                 JsonError *err)
{
    VkAttachmentDescription *attachmentDescriptions =
        malloc(attachments->used * sizeof(VkAttachmentDescription));

    JsonObject *attachment;

    //Parse attachments
    for (int i = 0; i < attachments->used; i++) {
        VkAttachmentDescription *att = &attachmentDescriptions[i];
        attachment = attachments->vals[i];

        //Static value
        att->flags = 0;

        //Parse values
        jsonGetInt(attachment, "format",  E(att->format), err);  ERRCHKZ();
        jsonGetInt(attachment, "samples", E(att->samples), err); ERRCHKZ();
        jsonGetInt(attachment, "loadOp",  E(att->loadOp), err);  ERRCHKZ();
        jsonGetInt(attachment, "storeOp", E(att->storeOp), err); ERRCHKZ();
        jsonGetInt(attachment, "stencilLoadOp", E(att->stencilLoadOp), err);
        ERRCHKZ();
        jsonGetInt(attachment, "stencilStoreOp", E(att->stencilStoreOp), err);
        ERRCHKZ();
        jsonGetInt(attachment, "initialLayout", E(att->initialLayout), err);
        ERRCHKZ();
        jsonGetInt(attachment, "finalLayout", E(att->finalLayout), err);
        ERRCHKZ();

        if (att->format == -1) { //Use the swapchain's format for this attribute
            att->format = _jeSwapchainFormat();
        }
    }

    return attachmentDescriptions;
}

static VkAttachmentReference *parseAttachmentReferences(
                                                        JsonArray *attachments,
                                                        JsonError *err)
{
    if(attachments->used == 0) return 0;

    VkAttachmentReference *attachmentReferences =
        malloc(attachments->used * sizeof(VkAttachmentReference));

    for (int j = 0; j < attachments->used; j++) {
        jsonGetUInt(attachments->vals[j], "attachment",
                    &attachmentReferences[j].attachment, err); ERRCHKZ();

        jsonGetInt(attachments->vals[j], "layout",
                   E(attachmentReferences[j].layout), err); ERRCHKZ();
    }

    return attachmentReferences;
}

static uint32_t *parsePreserveAttachments(JsonArray *attachments,
                                          JsonError *err)
{
    if(attachments->used == 0) return 0;

    uint32_t *ret = malloc(attachments->used * sizeof(uint32_t));

    for (int i = 0; i < attachments->used; i++) {
        ret[i] = (uint32_t) ((JsonNumber*) attachments->vals[i])->data;
    }

    return ret;
}

static VkSubpassDescription *parseSubpasses(JsonArray *subpasses,
                                            JsonError *err)
{
    VkSubpassDescription *subpassDescriptions =
        malloc(subpasses->used * sizeof(VkSubpassDescription));

    JsonObject *subpass;
    JsonArray *inputAttachments;
    JsonArray *colorAttachments;
    JsonArray *resolveAttachments;
    JsonArray *preserveAttachments;
    //JsonObject *depthStencilAttachment;

    for (int i = 0; i < subpasses->used; i++) {
        VkSubpassDescription *description = &subpassDescriptions[i];
        subpass = subpasses->vals[i];

        //Static info
        description->flags = 0;

        //Parse bind point
        jsonGetInt(subpass, "bindPoint", E(description->pipelineBindPoint),
                   err); ERRCHKZ();


        //Parse input attachments
        jsonGetArray(subpass, "inputAttachments", &inputAttachments, err);
        ERRCHKZ();
        description->inputAttachmentCount = inputAttachments->used;
        description->pInputAttachments =
            parseAttachmentReferences(inputAttachments, err); ERRCHKZ();

        //Parse color attachments
        jsonGetArray(subpass, "colorAttachments", &colorAttachments, err);
        ERRCHKZ();
        description->colorAttachmentCount = colorAttachments->used;
        description->pColorAttachments =
            parseAttachmentReferences(colorAttachments, err); ERRCHKZ();

        //Parse resolve attachments
        jsonGetArray(subpass, "resolveAttachments", &resolveAttachments, err);
        ERRCHKZ();
        description->pResolveAttachments =
            parseAttachmentReferences(resolveAttachments, err); ERRCHKZ();

        //Parse depth attachments
        jsonGetArray(subpass, "preserveAttachments", &preserveAttachments,
                     err); ERRCHKZ();
        description->preserveAttachmentCount = preserveAttachments->used;
        description->pPreserveAttachments =
            parsePreserveAttachments(preserveAttachments, err); ERRCHKZ();

        //Parse depth stencil attachment
        int index = jsonGetPairIndex(subpass, "depthStencilAttachment");
        description->pDepthStencilAttachment = 0;
        if (index != -1 && jsonGetType(subpass->pairs[index].val) != JSON_NULL){
            VkAttachmentReference *depthAtt =
                malloc(sizeof(VkAttachmentReference));

            jsonGetUInt(subpass->pairs[index].val, "attachment",
                        &depthAtt->attachment, err);
            ERRCHKZ();

            jsonGetInt(subpass->pairs[index].val, "layout",
                       E(depthAtt->layout), err);
            ERRCHKZ();
            description->pDepthStencilAttachment = depthAtt;
        }
    }

    return subpassDescriptions;
}

//TODO
static VkSubpassDependency *parseDependencies()
{
    VkSubpassDependency *dependencies = 0;

    return dependencies;
}

static void parseRenderpass(JsonObject *json, const char *name, VkDevice device,
                            JERenderPass *renderPass, JsonError *err)
{
    //Parse attachments
    JsonArray *attachments;
    VkAttachmentDescription *attachmentDescriptions;

    jsonGetArray(json, "attachments", &attachments, err); ERRCHK();
    attachmentDescriptions = parseAttachments(attachments, err);
    ERRCHK();

    //Parse subpasses
    JsonArray *subpasses;
    VkSubpassDescription *subpassDescriptions;

    jsonGetArray(json, "subpasses", &subpasses, err);   ERRCHK();
    subpassDescriptions = parseSubpasses(subpasses, err); ERRCHK();

    //Parse dependencies
    VkSubpassDependency *subpassDependencies;

    subpassDependencies = parseDependencies();

    //Create renderpass
    VkRenderPassCreateInfo info = {
        VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
        0,
        0,
        attachments->used,
        attachmentDescriptions,
        subpasses->used,
        subpassDescriptions,
        0,
        subpassDependencies
    };

    VkResult res = vkCreateRenderPass(device, &info, 0,
                                      &renderPass->vk);
    if (res != VK_SUCCESS) {
        JE_ERROR(err, "Couldn't create render pass for renderpass %s",
                 name);
    }

    //Cleanup Vulkan information structures
    free((VkAttachmentDescription*) info.pAttachments);

    for (int i = 0; i < subpasses->used; i++) {
        free((VkAttachmentReference*) subpassDescriptions[i].pInputAttachments);
        free((VkAttachmentReference*) subpassDescriptions[i].pColorAttachments);
        free((VkAttachmentReference*)
             subpassDescriptions[i].pResolveAttachments);
        free((VkAttachmentReference*)
             subpassDescriptions[i].pPreserveAttachments);
        free((VkAttachmentReference*)
             subpassDescriptions[i].pDepthStencilAttachment);
    }
    free((VkSubpassDescription*) info.pSubpasses);

    free((VkSubpassDependency*) info.pDependencies);

    //Set the renderPass name
    renderPass->name = malloc(strlen(name)+1);
    strcpy(renderPass->name, name);
}

void _jeRenderPassLoad(VkDevice device, JEError *jeerr)
{
    JsonError *err = (JsonError *) jeerr;
    JEDirectory *dir = jeFSUtilOpenDirectory("res/renderpass/");

    if (dir != NULL) {
        renderPasses = calloc(0, sizeof(JERenderPass));
        char file[256];

        while (jeFSUtilNextFileInDirectory(dir, file, JE_FSUTIL_TYPE_REG)) {
            renderPasses = realloc(renderPasses,
                                   ++numRenderPasses * sizeof(JERenderPass));

            JsonObject *json = jsonParseFile(file, err);
            if (jeErrorIsSet(jeerr)) return;

            char *name = jeFSUtilGetFilename(file);

            parseRenderpass(json, name, device,
                            &renderPasses[numRenderPasses - 1], err);
            if (jeErrorIsSet(jeerr)) return;

            free(name);
            jsonCleanup(json);
        }

        jeFSUtilCloseDirectory(dir);
    }

#ifndef JE_NO_DEFAULT_GRAPHICS
    for (int i = 0; i < JE_DEFAULT_RENDERPASS_COUNT; i++) {
        JsonObject *json = jsonParseString((char*) defaultSources[i], err);
        if (jeErrorIsSet(jeerr)) return;

        parseRenderpass(json, defaultNames[i], device, &defaultRenderPasses[i],
                        err);
        if (jeErrorIsSet(jeerr)) return;

        jsonCleanup(json);
    }
#endif
}

VkRenderPass jeRenderPassVk(JERenderPass *renderPass)
{
    return renderPass->vk;
}

void jeRenderPassBegin(JERenderPass *technique,
                       JECommandBuffer *commandBuffer,
                       JEFramebuffer *framebuffer)
{
    VkClearValue clearValues[2];
    clearValues[0].color.float32[0] = 0.0f;
    clearValues[0].color.float32[1] = 0.0f;
    clearValues[0].color.float32[2] = 0.0f;
    clearValues[0].color.float32[3] = 1.0f;
    clearValues[1].depthStencil.depth = 1.0f;
    clearValues[1].depthStencil.stencil = 0;
    VkRenderPassBeginInfo info = {
        VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
        0,
        technique->vk,
        framebuffer->vk,
        {
            {0, 0},
            {framebuffer->width, framebuffer->height}
        },
        2,
        clearValues
    };

    vkCmdBeginRenderPass(commandBuffer->vkHandle, &info,
                         VK_SUBPASS_CONTENTS_INLINE);
}

void jeRenderPassEnd(JERenderPass *technique,
                     JECommandBuffer *commandBuffer, JEFramebuffer *framebuffer)
{
    vkCmdEndRenderPass(commandBuffer->vkHandle);
}


void _jeRenderPassCleanup(VkDevice device)
{
    for (int i = 0; i < numRenderPasses; i++) {
        vkDestroyRenderPass(device, renderPasses[i].vk, 0);
        free(renderPasses[i].name);
    }

#ifndef JE_NO_DEFAULT_GRAPHICS
    for (int i = 0; i < JE_DEFAULT_RENDERPASS_COUNT; i++) {
        vkDestroyRenderPass(device, defaultRenderPasses[i].vk, 0);
        free(defaultRenderPasses[i].name);
    }
#endif

    free(renderPasses);
    numRenderPasses = 0;
}
