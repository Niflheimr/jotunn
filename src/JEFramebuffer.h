/* Copyright (C) 2017-2018 Sam Bazley and Arnau Bigas
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef JEFRAMEBUFFER_H
#define JEFRAMEBUFFER_H

#include <vulkan/vulkan.h>

#include "JEVkUtil.h"

typedef struct _JEFramebufferAttachment JEFramebufferAttachment;

typedef struct _JEFramebuffer {
    int attachmentCount;
    JEFramebufferAttachment* attachments;
    VkFramebuffer vk;
    int width, height;
} JEFramebuffer;

void _jeFramebufferSetSwapchainImageViews(VkDevice device, int count,
        VkImageView* views);

JEFramebuffer* _jeFramebufferCreate(const char* layout, int width, int height,
        JEError *err);

JEFramebuffer* _jeFramebufferCreatesc(const char* layout, int width, int height,
        JEError *err);

void _jeFramebufferCleanup(JEFramebuffer* framebuffer);

void _jeFramebufferCleanupsc(JEFramebuffer* framebuffers);

#endif /* JEFRAMEBUFFER_H */
