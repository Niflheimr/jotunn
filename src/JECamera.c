/* Copyright (C) 2017-2018 Sam Bazley and Arnau Bigas
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include "JECamera.h"

#include <string.h>

#include "JEWindow.h"
#include "JERender.h"
#include "JEVkUtil.h"
#include "JEFramebuffer.h"

#define JE_CAMERA_MAX 16
#define JE_CAMERA_UBO_SIZE 4*4*4 //4x4 matrix, 4 bytes (float)
#define JE_CAMERA_UBO_BINDING 0

struct _JECamera {
    int commandBufferCount;
    VkCommandBuffer *commandBuffers;
    JEFramebuffer   *framebuffers;
    VkDescriptorSet descriptorSet;
    VkBuffer viewbuffer;
    VkDeviceMemory viewmemory;
    void *mappedbuffer;
    int recorded;       //boolean flag
    int active;         //boolean flag
    jeVec3f pos;
    jeMat4f projection;
    jeMat4f view;
    VkRect2D drawArea;
    JECameraInfo info;
};

//Render variables
static VkDevice dev;
static VkPhysicalDevice physicalDev;
static VkCommandPool commandPool;

//Camera array
static int numCameras = 0;
static JECamera* cameras = 0;

//Used for cameras rendering to textures
static int numOffscreenCmdBuffers = 0;
static VkCommandBuffer *offscreenCmdBuffers = 0;

//Used for cameras rendering to the screen
static int numScreenCmdBuffers = 0;
static int screenViewCount = 0;
static VkImageView *screenViews = 0;
static VkCommandBuffer **screenCmdBuffers = 0;

//Information sent to the shaders
static VkDescriptorPool descriptorPool;
static VkDescriptorSetLayout layout;

void _jeCameraInit(VkDevice device, VkPhysicalDevice physicalDevice,
                   VkCommandPool cmdPool, JEError *err)
{
    dev = device;
    physicalDev = physicalDevice;
    commandPool = cmdPool;

    VkDescriptorPoolSize poolSize = {
        VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
        JE_CAMERA_MAX
    };

    VkDescriptorPoolCreateInfo poolInfo = {
        VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
        NULL,
        VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT,
        JE_CAMERA_MAX,
        1,
        &poolSize
    };

    VkResult res = vkCreateDescriptorPool(device, &poolInfo, NULL,
                                          &descriptorPool);
    if (res != VK_SUCCESS) {
        JE_VKERROR(err, "Couldn't create camera descriptor set pool", res);
    }

    //layout (binding = 0) struct JECamera {...};
    VkDescriptorSetLayoutBinding bindings[] = {
        {
            JE_CAMERA_UBO_BINDING,
            VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
            1,
            VK_SHADER_STAGE_VERTEX_BIT,
            NULL
        }
    };

    VkDescriptorSetLayoutCreateInfo layoutInfo = {
        VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
        NULL,
        0,
        1,
        bindings
    };

    res = vkCreateDescriptorSetLayout(device, &layoutInfo, NULL, &layout);
    if (res != VK_SUCCESS) {
        JE_VKERROR(err, "Couldn't create camera descriptor set layout", res);
    }
}

void _jeCameraUpdateSwapchainViews(int count, VkImageView *surfaceImageViews,
                                   VkDevice device, JEError *err)
{
    //Update image views
    screenViewCount = count;

    screenViews = surfaceImageViews;

    //Update command buffer vector
    if(screenCmdBuffers == 0) {
        screenCmdBuffers = malloc(count * sizeof(VkCommandBuffer*));
        for (int i = 0; i < count; i++) {
            screenCmdBuffers[i] = 0;
        }
    }

    //force update of existing cameras
    for(int i = 0; i < numCameras; i++) {
        JECamera *camera = &cameras[i];
        JECameraInfo *info = &camera->info;
        if (info->flags & JE_CAMERA_FLAG_SCREEN) {
            _jeFramebufferCleanupsc(camera->framebuffers);

            info->width  = jeWindowGetWidth();
            info->height = jeWindowGetHeight();

            camera->framebuffers = _jeFramebufferCreatesc(info->fbLayout,
                                                          info->width,
                                                          info->height, err);
            if (jeErrorIsSet(err)) return;

            jeMat4Perspective(info->fov * 3.14f / 180.0f,
                              info->width / (float) info->height, info->near,
                              info->far, &camera->projection);

            //force commandbuffer recording
            cameras->recorded = 0;
            for (int i = 0; i < count; i++) {
                vkResetCommandBuffer(camera->commandBuffers[i], 0);
            }
        }
    }

}

static void updateCameraBuffer(JECamera *camera)
{
    //might need synchronization
    jeMat4f mat;
    jeMat4Mul(&camera->projection, &camera->view, &mat);

    memcpy(camera->mappedbuffer, mat, JE_CAMERA_UBO_SIZE);

    VkMappedMemoryRange flushRange = {
        VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE,
        NULL,
        camera->viewmemory,
        0,
        VK_WHOLE_SIZE,
    };
    vkFlushMappedMemoryRanges(dev, 1, &flushRange);
}

JECamera* jeCameraAdd(JECameraInfo info, JEError *err)
{
    if (cameras == 0) {
        cameras = calloc(0, sizeof(JECamera));
    }

    cameras = realloc(cameras, ++numCameras * sizeof(JECamera));
    JECamera *camera = &cameras[numCameras - 1];

    camera->info = info;
    camera->recorded = 0;
    camera->active = 0;

    if (info.flags & JE_CAMERA_FLAG_SCREEN) {
        //Override dimensions to match the window's
        camera->info.width  = jeWindowGetWidth();
        camera->info.height = jeWindowGetHeight();

        camera->framebuffers = _jeFramebufferCreatesc(info.fbLayout, info.width,
                                                      info.height, err);
        if (jeErrorIsSet(err)) return NULL;

        camera->commandBufferCount = screenViewCount;
    } else {
        camera->framebuffers =
            _jeFramebufferCreate(info.fbLayout, info.width, info.height, err);
        camera->commandBufferCount = 1;
    }

    camera->commandBuffers =
        malloc(sizeof(VkCommandBuffer) * camera->commandBufferCount);

    VkCommandBufferAllocateInfo cmdInfo = {
        VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
        0,
        commandPool,
        VK_COMMAND_BUFFER_LEVEL_PRIMARY,
        camera->commandBufferCount
    };

    VkResult result = vkAllocateCommandBuffers(dev, &cmdInfo,
                                               camera->commandBuffers);
    if (result != VK_SUCCESS) {
        JE_ERROR(err, "Couldn't allocate command buffers");
    }

    VkDescriptorSetAllocateInfo allocInfo = {
        VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
        NULL,
        descriptorPool,
        1,
        &layout
    };

    result = vkAllocateDescriptorSets(dev, &allocInfo,
                                      &camera->descriptorSet);
    if (result != VK_SUCCESS) {
        JE_VKERROR(err, "Couldn't allocate descriptor set for camera", result);
    }

    VkBufferCreateInfo bufferInfo = {
        VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        NULL,
        0,
        JE_CAMERA_UBO_SIZE,
        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        VK_SHARING_MODE_EXCLUSIVE,
        0,
        NULL
    };

    result = vkCreateBuffer(dev, &bufferInfo, NULL, &camera->viewbuffer);
    if (result != VK_SUCCESS) {
        JE_VKERROR(err, "Couldn't create camera view buffer", result);
        return camera;
    }

    VkMemoryRequirements memReq;
    vkGetBufferMemoryRequirements(dev, camera->viewbuffer, &memReq);

    camera->viewmemory = jeVkUtilAllocateMemory(dev, memReq,
                                           VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
                                           VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                                           err);
    if (jeErrorIsSet(err)) return camera;

    result = vkBindBufferMemory(dev, camera->viewbuffer, camera->viewmemory, 0);
    if (result != VK_SUCCESS) {
        JE_VKERROR(err, "Couldn't bind model data buffer to memory", result);
    }

    result = vkMapMemory(dev, camera->viewmemory, 0, JE_CAMERA_UBO_SIZE, 0,
                         &camera->mappedbuffer);
    if (result != VK_SUCCESS) {
        JE_VKERROR(err, "Couldn't map memory", result);
        return camera;
    }

    VkDescriptorBufferInfo descriptorBufferInfo = {
        camera->viewbuffer,
        0,
        JE_CAMERA_UBO_SIZE
    };

    VkWriteDescriptorSet descriptorWrite = {
        VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
        NULL,
        camera->descriptorSet,
        JE_CAMERA_UBO_BINDING,
        0,
        1,
        VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
        NULL,
        &descriptorBufferInfo,
        NULL
    };

    vkUpdateDescriptorSets(dev, 1, &descriptorWrite, 0, NULL);

    jeMat4Perspective(info.fov * 3.14f / 180.0f,
                      info.width / (float) info.height, info.near, info.far,
                      &camera->projection);
    camera->pos = (jeVec3f) {{0.0f, 0.0f, 1.0f}};
    jeCameraLookAt(camera, (jeVec3f) {{0.0f, 0.0f, 0.0f}});

    return camera;
}

void jeCameraSetPosition(JECamera *camera, jeVec3f position)
{
    camera->pos = position;
    //TODO: update view matrix using angles and stuff
}

void jeCameraLookAt(JECamera *camera, jeVec3f position)
{
    //Vulkan uses the top left corner origin, so up is actually -y
    jeMat4LookAt(camera->pos, position,(jeVec3f) {{0, -1, 0}}, &camera->view);
    updateCameraBuffer(camera);
}

void jeCameraSetActive(JECamera *camera, int active)
{
    if (active && !camera->active) {
        if (camera->info.flags & JE_CAMERA_FLAG_SCREEN) {
            numScreenCmdBuffers++;
            for (int i = 0; i < screenViewCount; i++) {
                screenCmdBuffers[i] = realloc(screenCmdBuffers[i],
                                              numScreenCmdBuffers *
                                              sizeof(VkCommandBuffer));
                screenCmdBuffers[i][numScreenCmdBuffers - 1] =
                    camera->commandBuffers[i];
            }
        } else {
            int start = numOffscreenCmdBuffers;
            numOffscreenCmdBuffers += camera->commandBufferCount;
            offscreenCmdBuffers = realloc(offscreenCmdBuffers,
                                          numOffscreenCmdBuffers *
                                          sizeof(VkCommandBuffer));
            for (int i = 0; i < camera->commandBufferCount; i++) {
                offscreenCmdBuffers[start + i] =
                    camera->commandBuffers[i];
            }
        }
        camera->active = 1;
    } else {
        //TODO
    }
}

VkDescriptorSet jeCameraDescriptorSet(JECamera *camera)
{
    return camera->descriptorSet;
}

static void recordCommandBuffer(VkCommandBuffer commandBuffer,
                                JEFramebuffer *framebuffer,
                                JECameraCallback callback)
{

    VkCommandBufferBeginInfo info = {
        VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        0,
        VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT,
        0
    };

    vkBeginCommandBuffer(commandBuffer, &info);

    JECommandBuffer cmdBuffer = {commandBuffer, {0, 0, 0, 0}};

    callback(&cmdBuffer, framebuffer);

    VkResult result = vkEndCommandBuffer(commandBuffer);
    if (result != VK_SUCCESS) {
        jeLoggerWarning("There was an error while recording cmd buffers");
    }
}

void _jeCameraRecordCmdBuffers(int index)
{
    for (int i = 0; i < numCameras; i++) {
        JECamera *camera = &cameras[i];
        if (!camera->active) {
            continue;
        }

        if (camera->info.flags & JE_CAMERA_FLAG_RECORD_ONCE &&
            camera->recorded == camera->commandBufferCount) {
            continue;
        }

        if (camera->info.flags & JE_CAMERA_FLAG_SCREEN) {

            recordCommandBuffer(camera->commandBuffers[index],
                                &camera->framebuffers[index],
                                camera->info.callback);

            if (camera->info.flags & JE_CAMERA_FLAG_RECORD_ONCE) {
                camera->recorded++;
            }
        } else {

            for (int j = 0; j < camera->commandBufferCount; j++) {
                recordCommandBuffer(camera->commandBuffers[j],
                                    &camera->framebuffers[j],
                                    camera->info.callback);
            }

            if (camera->info.flags & JE_CAMERA_FLAG_RECORD_ONCE) {
                camera->recorded += camera->commandBufferCount;
            }
        }
    }
}

void _jeCameraSubmitCmdBuffers(VkDevice device, VkQueue queue, VkSemaphore wait,
                               VkSemaphore finish, VkFence presentFence,
                               int imageIndex)
{
    VkPipelineStageFlags waitDstStageMask =
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    VkSubmitInfo info = {
        VK_STRUCTURE_TYPE_SUBMIT_INFO,
        0,
        0,
        0,
        &waitDstStageMask,
        numOffscreenCmdBuffers,
        offscreenCmdBuffers,
        0,
        0
    };

    VkResult result;
    result = vkQueueSubmit(queue, 1, &info, VK_NULL_HANDLE);
    if (result != VK_SUCCESS) {
        jeLoggerWarning("There was an error while submitting offscreen camera "
                        "command buffers");
    }

    info.waitSemaphoreCount = 1;
    info.pWaitSemaphores = &wait;
    info.commandBufferCount = numScreenCmdBuffers;
    info.pCommandBuffers = screenCmdBuffers[imageIndex];
    info.signalSemaphoreCount = 1;
    info.pSignalSemaphores = &finish;

    result = vkQueueSubmit(queue, 1, &info, presentFence);
    if (result != VK_SUCCESS) {
        jeLoggerWarning("There was an error while submitting screen camera "
                        "command buffers");
    }
}

void _jeCameraCleanup(VkDevice device)
{
    for (int i = 0; i < numCameras; i++) {
        JECamera *camera = &cameras[i];

        if (camera->info.flags & JE_CAMERA_FLAG_SCREEN) {
            _jeFramebufferCleanupsc(camera->framebuffers);
        } else {
            _jeFramebufferCleanup(camera->framebuffers);
        }

        vkUnmapMemory(device, camera->viewmemory);
        vkFreeMemory(device, camera->viewmemory, NULL);
        vkDestroyBuffer(device, camera->viewbuffer, NULL);

        free(camera->commandBuffers);
    }
    free(cameras);

    vkDestroyDescriptorPool(device, descriptorPool, NULL);
    vkDestroyDescriptorSetLayout(device, layout, NULL);

    free(offscreenCmdBuffers);

    for (int i = 0; i < screenViewCount; i++) {
        free(screenCmdBuffers[i]);
    }
    free(screenCmdBuffers);
}
