/* Copyright (C) 2017-2018 Sam Bazley and Arnau Bigas
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include "JEKeyboard.h"
#include "JELogger.h"
#include "JEEvent.h"

static int scancodeStates [128] = {0};

void _jeKeyboardSetKeyState(jeKeyboardKey sc, jeKeyboardKey ks, char typed [4],
                            int down)
{
    scancodeStates[sc] = down;

    JEEventKeyboard ev;
    ev.type = JE_EVENT_KEYBOARD;

    if (typed[0] < 32) {
        ev.typed[0] = ev.typed[1] = ev.typed[2] = ev.typed[3] = 0;
    } else {
        ev.typed[0] = typed[0];
        ev.typed[1] = typed[1];
        ev.typed[2] = typed[2];
        ev.typed[3] = typed[3];
    }

    ev.scancode = sc;
    ev.keysym = ks;
    ev.down = down;

    _jeEventSubmit((JEEvent *) &ev);
}

int jeKeyboardIsScancodeDown(jeKeyboardKey sc)
{
    return scancodeStates[sc];
}

#ifdef __linux__
//xmodmap -pk
const int JE_KEYBOARD_OSTOSTD [] = {
    JE_KEY_NONE,           /* 0 */
    JE_KEY_ESCAPE,         /* 1 */
    JE_KEY_1,              /* 2 */
    JE_KEY_2,              /* 3 */
    JE_KEY_3,              /* 4 */
    JE_KEY_4,              /* 5 */
    JE_KEY_5,              /* 6 */
    JE_KEY_6,              /* 7 */
    JE_KEY_7,              /* 8 */
    JE_KEY_8,              /* 9 */
    JE_KEY_9,              /* 10 */
    JE_KEY_0,              /* 11 */
    JE_KEY_MINUS,          /* 12 */
    JE_KEY_EQUALS,         /* 13 */
    JE_KEY_BACKSPACE,      /* 14 */
    JE_KEY_TAB,            /* 15 */
    JE_KEY_Q,              /* 16 */
    JE_KEY_W,              /* 17 */
    JE_KEY_E,              /* 18 */
    JE_KEY_R,              /* 19 */
    JE_KEY_T,              /* 20 */
    JE_KEY_Y,              /* 21 */
    JE_KEY_U,              /* 22 */
    JE_KEY_I,              /* 23 */
    JE_KEY_O,              /* 24 */
    JE_KEY_P,              /* 25 */
    JE_KEY_LEFTBRACKET,    /* 26 */
    JE_KEY_RIGHTBRACKET,   /* 27 */
    JE_KEY_RETURN,         /* 28 */
    JE_KEY_LCTRL,          /* 29 */
    JE_KEY_A,              /* 30 */
    JE_KEY_S,              /* 31 */
    JE_KEY_D,              /* 32 */
    JE_KEY_F,              /* 33 */
    JE_KEY_G,              /* 34 */
    JE_KEY_H,              /* 35 */
    JE_KEY_J,              /* 36 */
    JE_KEY_K,              /* 37 */
    JE_KEY_L,              /* 38 */
    JE_KEY_SEMICOLON,      /* 39 */
    JE_KEY_APOSTROPHE,     /* 40 */
    JE_KEY_GRAVE,          /* 41 */
    JE_KEY_LSHIFT,         /* 42 */
    JE_KEY_BACKSLASH,      /* 43 */
    JE_KEY_Z,              /* 44 */
    JE_KEY_X,              /* 45 */
    JE_KEY_C,              /* 46 */
    JE_KEY_V,              /* 47 */
    JE_KEY_B,              /* 48 */
    JE_KEY_N,              /* 49 */
    JE_KEY_M,              /* 50 */
    JE_KEY_COMMA,          /* 51 */
    JE_KEY_PERIOD,         /* 52 */
    JE_KEY_SLASH,          /* 53 */
    JE_KEY_RSHIFT,         /* 54 */
    JE_KEY_NUM_MULTIPLY,   /* 55 */
    JE_KEY_LALT,           /* 56 */
    JE_KEY_SPACE,          /* 57 */
    JE_KEY_CAPSLOCK,       /* 58 */
    JE_KEY_F1,             /* 59 */
    JE_KEY_F2,             /* 60 */
    JE_KEY_F3,             /* 61 */
    JE_KEY_F4,             /* 62 */
    JE_KEY_F5,             /* 63 */
    JE_KEY_F6,             /* 64 */
    JE_KEY_F7,             /* 65 */
    JE_KEY_F8,             /* 66 */
    JE_KEY_F9,             /* 67 */
    JE_KEY_F10,            /* 68 */
    JE_KEY_NUMLOCK,        /* 69 */
    JE_KEY_SCROLLLOCK,     /* 70 */
    JE_KEY_NUM_7,          /* 71 */
    JE_KEY_NUM_8,          /* 72 */
    JE_KEY_NUM_9,          /* 73 */
    JE_KEY_NUM_MINUS,      /* 74 */
    JE_KEY_NUM_4,          /* 75 */
    JE_KEY_NUM_5,          /* 76 */
    JE_KEY_NUM_6,          /* 77 */
    JE_KEY_NUM_PLUS,       /* 78 */
    JE_KEY_NUM_1,          /* 79 */
    JE_KEY_NUM_2,          /* 80 */
    JE_KEY_NUM_3,          /* 81 */
    JE_KEY_NUM_0,          /* 82 */
    JE_KEY_NUM_PERIOD,     /* 83 */
    0,                     /* 84 */
    0,                     /* 85 */
    JE_KEY_NONUSBACKSLASH, /* 86 */
    JE_KEY_F11,            /* 87 */
    JE_KEY_F12,            /* 88 */
    0,                     /* 89 */
    0,                     /* 90 */
    0,                     /* 91 */
    0,                     /* 92 */
    0,                     /* 93 */
    0,                     /* 94 */
    0,                     /* 95 */
    JE_KEY_NUM_ENTER,      /* 96 */
    JE_KEY_RCTRL,          /* 97 */
    JE_KEY_NUM_DIVIDE,     /* 98 */
    JE_KEY_PRINTSCREEN,    /* 99 */
    JE_KEY_RALT,           /* 100 */
    0,                     /* 101 */
    JE_KEY_HOME,           /* 102 */
    JE_KEY_UP,             /* 103 */
    JE_KEY_PGUP,           /* 104 */
    JE_KEY_LEFT,           /* 105 */
    JE_KEY_RIGHT,          /* 106 */
    JE_KEY_END,            /* 107 */
    JE_KEY_DOWN,           /* 108 */
    JE_KEY_PGDOWN,         /* 109 */
    JE_KEY_INSERT,         /* 110 */
    JE_KEY_DELETE,         /* 111 */
    0,                     /* 112 */
    0,                     /* 113 */
    0,                     /* 114 */
    0,                     /* 115 */
    0,                     /* 116 */
    0,                     /* 117 */
    0,                     /* 118 */
    JE_KEY_PAUSE,          /* 119 */
    0,                     /* 120 */
    0,                     /* 121 */
    0,                     /* 122 */
    0,                     /* 123 */
    0,                     /* 124 */
    JE_KEY_LGUI,           /* 125 */
    JE_KEY_RGUI,           /* 126 */
    JE_KEY_APPLICATION     /* 127 */
};
#endif

const char *JE_KEYBOARD_KEY_NAMES [] = {
    0,
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
    "M",
    "N",
    "O",
    "P",
    "Q",
    "R",
    "S",
    "T",
    "U",
    "V",
    "W",
    "X",
    "Y",
    "Z",
    "0",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "Return",
    "Escape",
    "Backspace",
    "Tab",
    "Space",
    "-",
    "=",
    "[",
    "]",
    "\\",
    "#",
    ";",
    "'",
    "`",
    ",",
    ".",
    "/",
    "Caps Lock",
    "F1",
    "F2",
    "F3",
    "F4",
    "F5",
    "F6",
    "F7",
    "F8",
    "F9",
    "F10",
    "F11",
    "F12",
    "Print Screen",
    "Scroll Lock",
    "Pause",
    "Insert",
    "Home",
    "Page Up",
    "Delete",
    "End",
    "Page Down",
    "Right",
    "Left",
    "Down",
    "Up",
    "Num Lock",
    "Keypad /",
    "Keypad *",
    "Keypad -",
    "Keypad +",
    "Keypad Enter",
    "Keypad 1",
    "Keypad 2",
    "Keypad 3",
    "Keypad 4",
    "Keypad 5",
    "Keypad 6",
    "Keypad 7",
    "Keypad 8",
    "Keypad 9",
    "Keypad 0",
    "Keypad .",
    "\\",
    "Application",

    "Left Ctrl",
    "Left Shift",
    "Left Alt",
    "Left GUI",
    "Right Ctrl",
    "Right Shift",
    "Right Alt",
    "Right GUI"
};
