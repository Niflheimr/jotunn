/* Copyright (C) 2017-2018 Sam Bazley and Arnau Bigas
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include "JEVkUtil.h"

#include <string.h>

static VkQueue transferQueue;
static VkCommandPool transferCommandPool;
static VkPhysicalDeviceMemoryProperties memProps;

void _jeVkUtilInit(VkQueue queue, VkCommandPool commandPool,
                   VkPhysicalDevice device)
{
    transferQueue = queue;
    transferCommandPool = commandPool;
    vkGetPhysicalDeviceMemoryProperties(device, &memProps);
}

VkDeviceMemory jeVkUtilAllocateMemory(VkDevice dev, VkMemoryRequirements memReq,
                                      VkMemoryPropertyFlagBits memFlags,
                                      JEError *err)
{
    VkDeviceMemory memory;

    for(uint32_t i = 0; i < memProps.memoryTypeCount; i++) {
        if ((memReq.memoryTypeBits & (1 << i)) &&
            (memProps.memoryTypes[i].propertyFlags & memFlags)==memFlags) {
            VkMemoryAllocateInfo memAllocateInfo = {
                VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
                NULL,
                memReq.size,
                i
            };

            VkResult result = vkAllocateMemory(dev, &memAllocateInfo,
                                               NULL, &memory);
            if (result != VK_SUCCESS) {
                JE_VKERROR(err, "Couldn't allocate memory", result);
            }
            break;
        }
    }

    if (memory == 0) {
        JE_ERROR(err, "Couldn't find suitable memory for buffer");
    }

    return memory;
}

void jeVkUtilStageData(VkDevice dev, VkPhysicalDevice physicalDev,
                       VkBuffer dstBuffer, size_t size, void *dataSrc,
                       JEError *err)
{
    //Create staging buffer
    VkBuffer stagingBuffer;
    VkDeviceMemory stagingMemory;

    VkBufferCreateInfo bufferInfo = {
        VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        NULL,
        0,
        size,
        VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        VK_SHARING_MODE_EXCLUSIVE,
        0,
        NULL
    };

    VkResult res = vkCreateBuffer(dev, &bufferInfo, NULL, &stagingBuffer);
    if (res != VK_SUCCESS) {
        JE_VKERROR(err, "Couldn't create staging buffer", res);
        return;
    }

    //Allocate memory
    VkMemoryRequirements memReq;
    vkGetBufferMemoryRequirements(dev, stagingBuffer, &memReq);

    stagingMemory = jeVkUtilAllocateMemory(dev, memReq,
                                           VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
                                           VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                                           err);
    if (jeErrorIsSet(err)) return;

    res = vkBindBufferMemory(dev, stagingBuffer, stagingMemory, 0);
    if (res != VK_SUCCESS) {
        JE_VKERROR(err, "Couldn't bind staging buffer to memory", res);
    }

    //Upload data to transfer buffer
    void *dataDest;
    vkMapMemory(dev, stagingMemory, 0, size, 0, &dataDest);
    memcpy(dataDest, dataSrc, size);
    vkUnmapMemory(dev, stagingMemory);

    //Transfer the data to the final buffer
    VkCommandBufferAllocateInfo allocInfo = {
        VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
        NULL,
        transferCommandPool,
        VK_COMMAND_BUFFER_LEVEL_PRIMARY,
        1
    };

    VkCommandBuffer cmdBuffer;
    vkAllocateCommandBuffers(dev, &allocInfo, &cmdBuffer);

    VkCommandBufferBeginInfo beginInfo = {
        VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        NULL,
        VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
        NULL
    };

    vkBeginCommandBuffer(cmdBuffer, &beginInfo);

    VkBufferCopy copyRegion = {
        0, 0, size
    };

    vkCmdCopyBuffer(cmdBuffer, stagingBuffer, dstBuffer, 1, &copyRegion);

    vkEndCommandBuffer(cmdBuffer);

    VkSubmitInfo submitInfo = {
        VK_STRUCTURE_TYPE_SUBMIT_INFO,
        NULL,
        0,
        NULL,
        NULL,
        1,
        &cmdBuffer,
        0,
        NULL
    };

    vkQueueSubmit(transferQueue, 1, &submitInfo, VK_NULL_HANDLE);
    vkQueueWaitIdle(transferQueue);

    vkFreeMemory(dev, stagingMemory, NULL);
    vkDestroyBuffer(dev, stagingBuffer, NULL);
}
