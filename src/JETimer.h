/* Copyright (C) 2017-2018 Sam Bazley and Arnau Bigas
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef JETIMER_H
#define JETIMER_H

#ifdef __linux__
#   include <time.h>
#elif _WIN32

#else
#   error "Platform not supported by the engine"
#endif

typedef struct JETimer {
#ifdef __linux__
    struct timespec start;
#elif _WIN32

#else
#   error "Platform not supported by the engine"
#endif
} JETimer;

/**
 * Starts a timer. A timer that has already been started will be reset.
 * @param timer An empty JETimer struct
 */
void jeTimerStart(JETimer *timer);

int jeTimerElapsedSeconds(JETimer *timer);

int jeTimerElapsedMilliseconds(JETimer *timer);

long jeTimerElapsedMicroseconds(JETimer *timer);

#endif /* JETIMER_H */
