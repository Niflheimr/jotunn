/* Copyright (C) 2017-2018 Sam Bazley and Arnau Bigas
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include "JEPipeline.h"

#include <string.h>
#include <stdlib.h>

#include "ujson.h"
#include "JEShader.h"
#include "JEWindow.h"
#include "JERenderPass.h"
#include "JEVkUtil.h"
#include "JEFSUtil.h"

#ifndef MAX_FILEPATH_LENGTH //default max path length is 256
#   define MAX_FILEPATH_LENGTH 256
#endif

typedef struct JEDescriptorBinding {
    char *name;
    int binding;
    int size;
    VkShaderStageFlags stage;
    VkDescriptorType type;
} JEDescriptorBinding;

typedef struct JEDescriptorSet {
    char *name;
    int set;
    int numBindings;
    JEDescriptorBinding *bindings;
    VkDescriptorSetLayout layout;
} JEDescriptorSet;

typedef struct JEPushConstants {
    char *name;
    unsigned int offset;
    unsigned int size;
    VkShaderStageFlags stages;
} JEPushConstants;

struct _JEPipeline {
    char *name;
    VkPipeline handle;
    int numDescriptorSets;
    JEDescriptorSet *descriptorSets;
    int numPushConstants;
    JEPushConstants *pushConstants;
    VkPipelineLayout layout;
    int hasStaticWindowViewport;
};

#ifndef JE_NO_DEFAULT_GRAPHICS
#   define JE_DEFAULT_PIPELINE_COUNT 2
extern const char _binary_arrow_json_start;

static const char* defaultSources[] = {
    &_binary_arrow_json_start
};

static const char* defaultNames[] = {
    "arrow"
};

static JEPipeline defaultPipelines[JE_DEFAULT_PIPELINE_COUNT];
#endif

static void destroyPipeline(VkDevice device, JEPipeline *pipeline)
{
    vkDestroyPipeline(device, pipeline->handle, NULL);
    vkDestroyPipelineLayout(device, pipeline->layout, NULL);

    for (int j = 0; j < pipeline->numDescriptorSets; j++) {
        JEDescriptorSet *descriptorSet = &pipeline->descriptorSets[j];

        vkDestroyDescriptorSetLayout(device, descriptorSet->layout, NULL);

        for (int k = 0; k < descriptorSet->numBindings; k++) {
            free(descriptorSet->bindings[k].name);
        }
        free(descriptorSet->bindings);

        free(descriptorSet->name);
    }
    free(pipeline->descriptorSets);

    for (int j = 0; j < pipeline->numPushConstants; j++) {
        JEPushConstants *pushConstants = &pipeline->pushConstants[j];

        free(pushConstants->name);
    }
    free(pipeline->pushConstants);

    free(pipeline->name);
}


static JEPipeline *pipelines;
static unsigned int numPipelines;

//Used by the json parsing functions
#define ERRCHK() if (jeErrorIsSet((JEError *) err)) return;

//This is needed to parse ints as enums
#define E(X) (int*) &X

static VkPipelineShaderStageCreateInfo *parseShaders(JsonArray *shaders,
                                                     JsonError *err)
{
    //Allocate shader infos
    VkPipelineShaderStageCreateInfo *shaderInfos =
        malloc(shaders->used * sizeof(VkPipelineShaderStageCreateInfo));

    for (int i = 0; i < shaders->used; i++) {
        VkPipelineShaderStageCreateInfo *shaderInfo = &shaderInfos[i];
        JEShader *shader = 0;

        JsonType type = jsonGetType(shaders->vals[i]);
        if (type == JSON_STRING) {
            shader = jeShaderGet(((JsonString*) shaders->vals[i])->data);
        } else if (type == JSON_NUMBER) {
#ifndef JE_NO_DEFAULT_GRAPHICS
            shader =
                jeShaderGetDefault((int)((JsonNumber*) shaders->vals[i])->data);
#endif
        }

        if (shader == 0) {
            JE_ERROR(err, "Couldn't find shader for pipeline");
            return 0;
        }

        //Pass the information to Vulkan
        shaderInfo->sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        shaderInfo->pNext = 0;
        shaderInfo->flags = 0;
        shaderInfo->stage = shader->stage;
        shaderInfo->module = shader->module;
        shaderInfo->pName = "main";
        shaderInfo->pSpecializationInfo = 0;
    }

    return shaderInfos;
}

static void parseVertexInputState(JsonObject *json,
                                  VkPipelineVertexInputStateCreateInfo *vis,
                                  JsonError *err)
{
    JsonArray *bindings;
    jsonGetArray(json, "bindings", &bindings, err); ERRCHK();

    //Allocate space for the binding information
    VkVertexInputBindingDescription *vbDescs = 0;
    vbDescs = malloc(bindings->used * sizeof(VkVertexInputBindingDescription));

    JsonObject *binding;

    //Parse bindings
    for (int i = 0; i < bindings->used; i++) {
        VkVertexInputBindingDescription *bd = &vbDescs[i];

        binding = bindings->vals[i];

        //Parse values and pass them to Vulkan
        jsonGetUInt(binding, "id", &bd->binding, err);           ERRCHK();
        jsonGetUInt(binding, "stride", &bd->stride, err);        ERRCHK();
        jsonGetInt(binding, "inputRate", E(bd->inputRate), err); ERRCHK();
    }

    JsonArray *attributes;
    jsonGetArray(json, "attributes", &attributes, err); ERRCHK();

    //Allocate space for the attribute description
    VkVertexInputAttributeDescription *vaDescs = 0;
    vaDescs =
        malloc(attributes->used * sizeof(VkVertexInputAttributeDescription));

    JsonObject *attribute;

    //Parse attributes
    for (int i = 0; i < attributes->used; i++) {
        VkVertexInputAttributeDescription *ad = &vaDescs[i];

        attribute = attributes->vals[i];

        //Parse values and pass them to Vulkan
        jsonGetUInt(attribute, "location", &ad->location, err); ERRCHK();
        jsonGetUInt(attribute, "binding", &ad->binding, err);   ERRCHK();
        jsonGetUInt(attribute, "offset", &ad->offset, err);     ERRCHK();
        jsonGetInt(attribute, "format", E(ad->format), err);    ERRCHK();
    }

    //Fill the Vulkan structure with the parsed info
    vis->sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    vis->pNext = 0;
    vis->flags = 0;
    vis->vertexBindingDescriptionCount = bindings->used;
    vis->pVertexBindingDescriptions = vbDescs;
    vis->vertexAttributeDescriptionCount = attributes->used;
    vis->pVertexAttributeDescriptions = vaDescs;
}

static void parseInputAssemblyState(JsonObject *json,
                                    VkPipelineInputAssemblyStateCreateInfo *inAssembly,
                                    JsonError *err)
{
    //Fill the structure's static info
    inAssembly->sType =
        VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    inAssembly->pNext = 0;
    inAssembly->flags = 0;

    //Parse the values and pass them to vulkan
    jsonGetInt(json, "topology", E(inAssembly->topology), err); ERRCHK();
    jsonGetBool(json, "primitiveRestart",
                &inAssembly->primitiveRestartEnable, 1, err); ERRCHK();
}

static void parseViewportState(JsonObject *json, int *isWindowViewport,
                               VkPipelineViewportStateCreateInfo *vs,
                               JsonError *err)
{
    int sizeType;
    jsonGetInt(json, "sizeType", &sizeType, err); ERRCHK();

    //Only one viewport/scissor supported for now
    VkViewport *viewport = malloc(sizeof(VkViewport));
    VkRect2D *scissor    = malloc(sizeof(VkRect2D));

    if (sizeType == 0) { //Use the values of the current window
        viewport->x = 0;
        viewport->y = 0;

        viewport->width = jeWindowGetWidth();
        viewport->height = jeWindowGetHeight();

        viewport->minDepth = 0.0f;
        viewport->maxDepth = 1.0f;


        scissor->offset.x = 0;
        scissor->offset.y = 0;

        scissor->extent.width = jeWindowGetWidth();
        scissor->extent.height = jeWindowGetHeight();

        *isWindowViewport = 1;
    } else if (sizeType == 1) { //Use the values stored in the configuration

    } else if (sizeType == 2) { //Use the values stored in the json file
        jsonGetFloat(json, "x", &viewport->x, err);           ERRCHK();
        jsonGetFloat(json, "y", &viewport->y, err);           ERRCHK();

        jsonGetFloat(json, "width", &viewport->width, err);   ERRCHK();
        jsonGetFloat(json, "height", &viewport->height, err); ERRCHK();

        viewport->minDepth = 0.1f;
        viewport->maxDepth = 100.0f;


        scissor->offset.x = viewport->x;
        scissor->offset.y = viewport->y;

        scissor->extent.width = viewport->width;
        scissor->extent.height = viewport->height;
    } else {
        JE_ERROR(err, "Wrong sizeType (%d) for pipeline", sizeType);
        return;
    }

    //Fill structure with Vulkan info
    vs->sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    vs->pNext = 0;
    vs->flags = 0;
    vs->viewportCount = 1;
    vs->pViewports = viewport;
    vs->scissorCount = 1;
    vs->pScissors = scissor;
}

static void parseRasterizationState(JsonObject *json,
                                    VkPipelineRasterizationStateCreateInfo *rs,
                                    JsonError *err)
{
    //Fill in the static data
    rs->sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    rs->pNext = 0;
    rs->flags = 0;
    rs->depthBiasEnable = VK_FALSE;
    rs->depthBiasConstantFactor = 0.0f;
    rs->depthBiasClamp = 0.0f;
    rs->depthBiasSlopeFactor = 0.0f;
    rs->lineWidth = 1.0f; //Might want to add JSON support for these later on

    jsonGetBool(json, "depthClamping", &rs->depthClampEnable, 1, err);
    ERRCHK();

    jsonGetBool(json, "rasterDiscard", &rs->rasterizerDiscardEnable, 1, err);
    ERRCHK();

    jsonGetInt(json, "polyMode", E(rs->polygonMode), err);  ERRCHK();
    jsonGetUInt(json, "cullingMode", &rs->cullMode, err);        ERRCHK();
    jsonGetInt(json, "frontFace", E(rs->frontFace), err);   ERRCHK();
}

static void parseMultisampleState(JsonObject *json,
                                  VkPipelineMultisampleStateCreateInfo *ms,
                                  JsonError *err)
{
    //Fill static info
    ms->sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    ms->pNext = 0;
    ms->flags = 0;
    ms->pSampleMask = 0;

    //Parse values
    jsonGetBool(json, "enable", &ms->sampleShadingEnable, 1, err); ERRCHK();
    jsonGetFloat(json, "min", &ms->minSampleShading, err);         ERRCHK();
    jsonGetBool(json, "ATOEnable", &ms->alphaToOneEnable, 1, err); ERRCHK();
    jsonGetBool(json, "ATCEnable", &ms->alphaToCoverageEnable, 1, err);
    ERRCHK();
    jsonGetInt(json, "samples", (int*) &ms->rasterizationSamples, err);
    ERRCHK();
}

static void parseDepthStencilState(JsonObject *json,
                                   VkPipelineDepthStencilStateCreateInfo *ds,
                                   JsonError *err)
{
    ds->sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    ds->pNext = 0;
    ds->flags = 0;

    jsonGetBool(json, "test", &ds->depthTestEnable, 1, err);         ERRCHK();
    jsonGetBool(json, "write", &ds->depthWriteEnable, 1, err);       ERRCHK();
    jsonGetUInt(json, "compareOp", &ds->depthCompareOp, err);        ERRCHK();
    jsonGetBool(json, "bounds", &ds->depthBoundsTestEnable, 1, err); ERRCHK();
    jsonGetBool(json, "stencil", &ds->stencilTestEnable, 1, err);    ERRCHK();
    ds->front.failOp = 0;
    ds->front.passOp = 0;
    ds->front.depthFailOp = 0;
    ds->front.compareOp = 0;
    ds->front.compareMask = 0;
    ds->front.writeMask = 0;
    ds->front.reference = 0;
    ds->back.failOp = 0;
    ds->back.passOp = 0;
    ds->back.depthFailOp = 0;
    ds->back.compareOp = 0;
    ds->back.compareMask = 0;
    ds->back.writeMask = 0;
    ds->back.reference = 0;
    jsonGetFloat(json, "minDepth", &ds->minDepthBounds, err);        ERRCHK();
    jsonGetFloat(json, "maxDepth", &ds->maxDepthBounds, err);        ERRCHK();
}

static void parseColorBlendState(JsonObject *json,
                                 VkPipelineColorBlendStateCreateInfo *cbs,
                                 JsonError *err)
{
    //Fill static info
    cbs->sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    cbs->pNext = 0;
    cbs->flags = 0;
    cbs->attachmentCount = 1; //Only support one attachment for now

    //Parse values
    jsonGetBool(json, "logic", &cbs->logicOpEnable, 1, err); ERRCHK();
    jsonGetInt(json, "logicOp", (int*) &cbs->logicOp, err);  ERRCHK();

    JsonObject *obj;
    jsonGetObject(json, "blendAttachment", &obj, err); ERRCHK();

    //Allocate blend attachments
    VkPipelineColorBlendAttachmentState *att =
        malloc(sizeof(VkPipelineColorBlendAttachmentState));

    //Fill in static info
    att->colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT |
        VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;

    //Parse values
    jsonGetBool(obj, "enable" , &att->blendEnable, 1, err); ERRCHK();
    jsonGetInt(obj, "colorSrc", E(att->srcColorBlendFactor), err); ERRCHK();
    jsonGetInt(obj, "colorDst", E(att->dstColorBlendFactor), err); ERRCHK();
    jsonGetInt(obj, "colorOp" , E(att->colorBlendOp       ), err); ERRCHK();
    jsonGetInt(obj, "alphaSrc", E(att->srcAlphaBlendFactor), err); ERRCHK();
    jsonGetInt(obj, "alphaDst", E(att->dstAlphaBlendFactor), err); ERRCHK();
    jsonGetInt(obj, "alphaOp" , E(att->alphaBlendOp       ), err); ERRCHK();

    cbs->pAttachments = att;

    JsonArray *blendConstants;
    jsonGetArray(json, "blendConstants", &blendConstants, err); ERRCHK();

    cbs->blendConstants[0] = ((JsonNumber*) blendConstants->vals[0])->data;
    cbs->blendConstants[1] = ((JsonNumber*) blendConstants->vals[1])->data;
    cbs->blendConstants[2] = ((JsonNumber*) blendConstants->vals[2])->data;
    cbs->blendConstants[3] = ((JsonNumber*) blendConstants->vals[3])->data;
}

static void parseDescriptorSets(JEPipeline *pipeline, JsonArray *json,
                                VkDevice device, VkDescriptorSetLayout *layouts,
                                JsonError *err)
{
    VkResult res;

    //Allocate space for the Descriptor Sets
    pipeline->numDescriptorSets = json->used;
    pipeline->descriptorSets = malloc(pipeline->numDescriptorSets *
                                      sizeof(JEDescriptorSet));

    //Variables used in the loops
    JsonObject *obj;
    JsonArray *jsonBindings;
    char *name;

    //Loop through all the Descriptor Sets
    for (int i = 0; i < pipeline->numDescriptorSets; i++) {
        obj = json->vals[i];
        JEDescriptorSet *descriptorSet = &pipeline->descriptorSets[i];

        //Store the Descriptor Set set number
        jsonGetInt(obj, "set", &descriptorSet->set, err); ERRCHK();

        //Store the name of the Descriptor Set
        jsonGetString(obj, "name", &name, err); ERRCHK();
        descriptorSet->name = malloc(strlen(name) + 1);
        strcpy(descriptorSet->name, name);

        jsonGetArray(obj, "bindings", &jsonBindings, err); ERRCHK();

        //Allocate space for the Bindings in this Descriptor Set
        descriptorSet->numBindings = jsonBindings->used;
        descriptorSet->bindings = malloc(descriptorSet->numBindings *
                                         sizeof(JEDescriptorBinding));

        //Loop through the bindings of this Descriptor Set
        VkDescriptorSetLayoutBinding bindings[jsonBindings->used];
        for (int j = 0; j < jsonBindings->used; j++) {
            obj = jsonBindings->vals[j];
            JEDescriptorBinding *binding = &descriptorSet->bindings[j];

            //Store the name of this binding
            jsonGetString(obj, "name", &name, err); ERRCHK();
            binding->name = malloc(strlen(name) + 1);
            strcpy(binding->name, name);

            //Parse the values
            jsonGetInt(obj, "binding", &binding->binding, err);   ERRCHK();
            jsonGetInt(obj, "size", &binding->size, err);         ERRCHK();
            jsonGetInt(obj, "type", E(binding->type), err);       ERRCHK();
            jsonGetUInt(obj, "stage", &binding->stage, err);      ERRCHK();

            //Pass them to Vulkan
            bindings[j].binding = binding->binding;
            bindings[j].descriptorType = binding->type;
            bindings[j].descriptorCount = 1;
            bindings[j].stageFlags = binding->stage;
            bindings[j].pImmutableSamplers = 0;
        }

        //Assemble the Vulkan information of this Descriptor Set
        VkDescriptorSetLayoutCreateInfo dslInfo = {
            VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
            NULL,
            0,
            descriptorSet->numBindings,
            bindings
        };

        //Create the Layout of this Descriptor Set
        res = vkCreateDescriptorSetLayout(device, &dslInfo, NULL,
                                          &descriptorSet->layout);
        if (res != VK_SUCCESS) {
            JE_VKERROR(err, "Couldn't create pipeline descriptor set layout",
                       res);
            return;
        }

        //Add the layout to the list of layouts of the Pipeline
        layouts[i] = descriptorSet->layout;
    };

}

static void parsePushConstants(JEPipeline *pipeline, JsonArray *pushConstants,
                               VkPushConstantRange *ranges, JsonError *err)
{
    pipeline->numPushConstants = pushConstants->used;
    pipeline->pushConstants =
        malloc(sizeof(JEPushConstants) * pushConstants->used);

    for (int i = 0; i < pushConstants->used; i++) {
        jsonGetInt(pushConstants->vals[i], "stages", E(ranges[i].stageFlags),
                   err); ERRCHK();
        pipeline->pushConstants[i].stages = ranges[i].stageFlags;

        jsonGetUInt(pushConstants->vals[i], "offset", &ranges[i].offset, err);
        ERRCHK();
        pipeline->pushConstants[i].offset = ranges[i].offset;

        jsonGetUInt(pushConstants->vals[i], "size", &ranges[i].size, err);
        ERRCHK();
        pipeline->pushConstants[i].size = ranges[i].size;

        char *name;
        jsonGetString(pushConstants->vals[i], "name", &name, err); ERRCHK();
        pipeline->pushConstants[i].name = malloc(strlen(name) + 1);
        strcpy(pipeline->pushConstants[i].name, name);
    }
}

static void parsePipelineLayout(JEPipeline *pipeline, JsonObject *json,
                                VkDevice device, JsonError *err)
{
    VkResult res;
    JsonArray *array;

    jsonGetArray(json, "descriptorSets", &array, err); ERRCHK();
    VkDescriptorSetLayout descriptors[array->used];
    parseDescriptorSets(pipeline, array, device, descriptors, err); ERRCHK();

    jsonGetArray(json, "pushConstants", &array, err); ERRCHK();
    VkPushConstantRange ranges[array->used];
    parsePushConstants(pipeline, array, ranges, err); ERRCHK();

    //Assemble the Vulkan information of all the Descriptor Sets of the Pipeline
    VkPipelineLayoutCreateInfo pl = {
        VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
        NULL,
        0,
        pipeline->numDescriptorSets,
        descriptors,
        pipeline->numPushConstants,
        ranges
    };

    //Create the Pipeline's layout
    res = vkCreatePipelineLayout(device, &pl, NULL, &pipeline->layout);
    if (res != VK_SUCCESS) {
        JE_VKERROR(err, "Couldn't create pipeline layout", res);
    }
}
#undef GETNUM

static void parsePipeline(JsonObject *json, JEPipeline *pipeline,
                          const char *name, VkDevice device, JsonError *err)
{
    JsonObject *obj;
    JsonArray *shaders;

    VkPipelineShaderStageCreateInfo *shaderInfos;
    jsonGetArray(json, "shaders", &shaders, err); ERRCHK();
    shaderInfos = parseShaders(shaders, err);       ERRCHK();

    VkPipelineVertexInputStateCreateInfo vertexInputState;
    jsonGetObject(json, "vertexInputState", &obj, err); ERRCHK();
    parseVertexInputState(obj, &vertexInputState, err);   ERRCHK();

    VkPipelineInputAssemblyStateCreateInfo inputAssemblyState;
    jsonGetObject(json, "inputAssemblyState", &obj, err); ERRCHK();
    parseInputAssemblyState(obj, &inputAssemblyState, err); ERRCHK();

    //Tessellation State (TODO)
    VkPipelineTessellationStateCreateInfo *tessellationState = 0;

    int *windowViewport = &pipeline->hasStaticWindowViewport;
    VkPipelineViewportStateCreateInfo viewportState;
    jsonGetObject(json, "viewportState", &obj, err);              ERRCHK();
    parseViewportState(obj, windowViewport, &viewportState, err); ERRCHK();

    VkPipelineRasterizationStateCreateInfo rasterizationState;
    jsonGetObject(json, "rasterizationState", &obj, err); ERRCHK();
    parseRasterizationState(obj, &rasterizationState, err); ERRCHK();

    VkPipelineMultisampleStateCreateInfo multisampleState;
    jsonGetObject(json, "multisampleState", &obj, err); ERRCHK();
    parseMultisampleState(obj, &multisampleState, err);   ERRCHK();

    VkPipelineDepthStencilStateCreateInfo depthStencilState;
    jsonGetObject(json, "depthStencilState", &obj, err);  ERRCHK();
    parseDepthStencilState(obj, &depthStencilState, err);   ERRCHK();

    VkPipelineColorBlendStateCreateInfo colorBlendState;
    jsonGetObject(json, "colorBlendState", &obj, err); ERRCHK();
    parseColorBlendState(obj, &colorBlendState, err);    ERRCHK();

    //Dynamic State (TODO)
    VkPipelineDynamicStateCreateInfo *dynamicState = 0;

    jsonGetObject(json, "layout", &obj, err);        ERRCHK();
    parsePipelineLayout(pipeline, obj, device, err); ERRCHK();

    //Retrieve the Render Pass to be used in conjunction with this Pipeline
    JsonType type = jsonGetMemberType(json, "renderPass");
    JERenderPass *renderPass = NULL;
    if (type == JSON_STRING) {
        char *renderPassName;
        jsonGetString(json, "renderPass", &renderPassName, err); ERRCHK();
        renderPass = jeRenderPassGet(renderPassName);
    } else if (type == JSON_NUMBER) {
#ifndef JE_NO_DEFAULT_GRAPHICS
        int renderPassID;
        jsonGetInt(json, "renderPass", &renderPassID, err); ERRCHK();
        renderPass = jeRenderPassGetDefault(renderPassID);
#endif
    }

    if (renderPass == NULL) {
        JE_ERROR(err, "Couldn't find render pass for pipeline %s", name);
        return;
    }

    int subpass;
    jsonGetInt(json, "subpass", &subpass, err); ERRCHK();

    //Assemble the Vulkan information for the Pipeline creation
    VkGraphicsPipelineCreateInfo info = {
        VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
        NULL,
        0,
        shaders->used,
        shaderInfos,
        &vertexInputState,
        &inputAssemblyState,
        tessellationState,
        &viewportState,
        &rasterizationState,
        &multisampleState,
        &depthStencilState,
        &colorBlendState,
        dynamicState,
        pipeline->layout,
        jeRenderPassVk(renderPass),
        subpass,
        VK_NULL_HANDLE,
        -1
    };

    //Create the Vulkan Pipeline itself
    VkResult res = vkCreateGraphicsPipelines(device, VK_NULL_HANDLE, 1, &info,
                                             0, &pipeline->handle);
    if (res != VK_SUCCESS) {
        JE_ERROR(err, "Couldn't create pipeline %s", name);
    }

    //Free Vulkan Pipeline creation information arrays
    free(shaderInfos);
    free((VkVertexInputBindingDescription*)
         vertexInputState.pVertexBindingDescriptions);
    free((VkVertexInputAttributeDescription*)
         vertexInputState.pVertexAttributeDescriptions);
    free((VkViewport*) viewportState.pViewports);
    free((VkRect2D*) viewportState.pScissors);
    free((VkPipelineColorBlendAttachmentState*) colorBlendState.pAttachments);

    pipeline->name = malloc(strlen(name)+1);
    strcpy(pipeline->name, name);
}

void _jePipelineLoad(VkDevice device, JEError *jeerr)
{
    JsonError *err = (JsonError *) jeerr;

    JEDirectory *dir = jeFSUtilOpenDirectory("res/pipeline/");

    if (dir != NULL) {
        pipelines = calloc(0, sizeof(JEPipeline));
        char file[MAX_FILEPATH_LENGTH];

        while (jeFSUtilNextFileInDirectory(dir, file, JE_FSUTIL_TYPE_REG)) {
            JsonObject *json = jsonParseFile(file, err);
            if (jeErrorIsSet(jeerr)) return;

            pipelines = realloc(pipelines, ++numPipelines * sizeof(JEPipeline));

            char *name = jeFSUtilGetFilename(file);

            parsePipeline(json, &pipelines[numPipelines-1], name, device, err);
            if (jeErrorIsSet(jeerr)) return;

            jsonCleanup(json);
        }

        jeFSUtilCloseDirectory(dir);
    }

#ifndef JE_NO_DEFAULT_GRAPHICS
    for (int i = 0; i < JE_DEFAULT_PIPELINE_COUNT; i++) {
        JsonObject *json = jsonParseString((char*) defaultSources[i], err);
        if (jeErrorIsSet(jeerr)) return;

        parsePipeline(json, &defaultPipelines[i], defaultNames[i], device, err);
        if (jeErrorIsSet(jeerr)) return;

        jsonCleanup(json);
    }
#endif
}

void _jePipelineResizeStaticViewports(VkDevice device, JEError *err)
{
    JsonError *jerr = (JsonError *) err;
    char file[MAX_FILEPATH_LENGTH];
    for (int i = 0; i < numPipelines; i++) {
        if (pipelines[i].hasStaticWindowViewport) {
            strcpy(file, "res/pipeline/");
            strcat(file, pipelines[i].name);
            strcat(file, ".json");
            char *name = malloc(strlen(pipelines[i].name)+1);
            strcpy(name, pipelines[i].name);
            destroyPipeline(device, &pipelines[i]);
            JsonObject *json = jsonParseFile(file, jerr);
            if (jeErrorIsSet(err)) {
                return;
            }
            parsePipeline(json, &pipelines[i], name, device, jerr);
            if (jeErrorIsSet(err)) {
                return;
            }
            free(name);
        }
    }
#ifndef JE_NO_DEFAULT_GRAPHICS
    for (int i = 0; i < JE_DEFAULT_PIPELINE_COUNT; i++) {
        if (defaultPipelines[i].hasStaticWindowViewport) {
            destroyPipeline(device, &defaultPipelines[i]);

            JsonObject *json = jsonParseString((char*) defaultSources[i], jerr);
            if (jeErrorIsSet(err)) return;

            parsePipeline(json, &defaultPipelines[i], defaultNames[i], device,
                          jerr);
            if (jeErrorIsSet(err)) return;

            jsonCleanup(json);
        }
    }
#endif
}

JEPipeline *jePipelineGet(const char *name)
{
    for (int i = 0; i < numPipelines; i++) {
        if (strcmp(pipelines[i].name, name) == 0) {
            return &pipelines[i];
        }
    }
    return NULL;
}

#ifndef JE_NO_DEFAULT_GRAPHICS
JEPipeline *jePipelineGetDefault(int id)
{
    return &defaultPipelines[id];
}
#endif

static JEDescriptorSet *getDescriptorSet(JEPipeline *pipeline, const char *name)
{
    for (int i = 0; i < pipeline->numDescriptorSets; i++) {
        if (strcmp(pipeline->descriptorSets[i].name, name) == 0) {
            return &pipeline->descriptorSets[i];
        }
    }
    return NULL;
}

int jePipelinePushConstantsIndex(JEPipeline *pipeline, const char *name)
{
    for (int i = 0; i < pipeline->numPushConstants; i++) {
        if (strcmp(pipeline->pushConstants[i].name, name) == 0) {
            return i;
        }
    }
    return -1;
}

void jePipelinePushConstantsUpdate(JEPipeline *pipeline, JECommandBuffer *cmd,
                                   int index, const void *data)
{
    JEPushConstants *pushConsts = &pipeline->pushConstants[index];
    vkCmdPushConstants(cmd->vkHandle, pipeline->layout, pushConsts->stages,
                       pushConsts->offset, pushConsts->size, data);
    cmd->stats.pushConstsBytes += pushConsts->size;
}

void jePipelineBind(JEPipeline *pipeline, JECommandBuffer *cmdBuffer)
{
    vkCmdBindPipeline(cmdBuffer->vkHandle, VK_PIPELINE_BIND_POINT_GRAPHICS,
                      pipeline->handle);
    cmdBuffer->stats.pipelineBinds += 1;
}

//TODO: Add support for Dynamic Uniform Buffers
void jePipelineBindDescriptorSet(JEPipeline *pipeline,
                                 JECommandBuffer *cmdBuffer, const char *dsName,
                                 VkDescriptorSet descriptorSet)
{
    JEDescriptorSet *ds = getDescriptorSet(pipeline, dsName);
    if (ds == NULL) {
        jeLoggerWarning("No Descriptor Set named %s found for Pipeline %s",
                        dsName, pipeline->name);
    }

    vkCmdBindDescriptorSets(cmdBuffer->vkHandle,
                            VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline->layout,
                            ds->set, 1, &descriptorSet, 0, 0);
}

void _jePipelineCleanup(VkDevice device)
{
    for (int i = 0; i < numPipelines; i++) {
        destroyPipeline(device, &pipelines[i]);
    }

#ifndef JE_NO_DEFAULT_GRAPHICS
    for (int i = 0; i < JE_DEFAULT_PIPELINE_COUNT; i++) {
        destroyPipeline(device, &defaultPipelines[i]);
    }
#endif

    free(pipelines);
    numPipelines = 0;
}
