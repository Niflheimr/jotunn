/* Copyright (C) 2017-2018 Sam Bazley and Arnau Bigas
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include "JEWindow.h"
#include "JEGame.h"
#include "JEKeyboard.h"
#include "JEEvent.h"
#include "JERender.h"
#include <stdlib.h>
#include <string.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h>
#include <X11/extensions/Xfixes.h>

static Display *display;
static Window window;

JEWindow _jeWindowHandle()
{
    JEWindow handle;
    handle.display = display;
    handle.window = window;
    return handle;
}

static int screen;

static Atom wmDeleteMessage;

static int width;
static int height;
static long flags;
static char *title;

static void pollMouseInput()
{
    Window root;
    Window child;
    int rx, ry, x, y;
    unsigned int mask;

    static int px, py;

    XQueryPointer(display, window, &root, &child, &rx, &ry, &x, &y, &mask);

    if (x < 0 || y < 0 || x > width || y > height) {
        return;
    }

    if (x == px && y == py) {
        return;
    }

    JEEventMouseMove ev;
    ev.type = JE_EVENT_MOUSE_MOVE;
    ev.dx = x - px;
    ev.dy = y - py;

    if (flags & JE_WINDOW_GRAB_CURSOR) {
        XWarpPointer(display, window, window, 0, 0, 0, 0, width/2, height/2);
        px = width/2;
        py = height/2;

        ev.x = 0;
        ev.y = 0;
    } else {
        px = x;
        py = y;

        ev.x = x;
        ev.y = y;
    }

    _jeEventSubmit((JEEvent *) &ev);
}

static void keyEvent(XEvent event, int down)
{
    XKeyEvent e = event.xkey;

    if (!down && XEventsQueued(display, QueuedAfterReading)) {
        XEvent nev;
        XPeekEvent(display, &nev);

        if (nev.type == KeyPress && nev.xkey.time == e.time &&
            nev.xkey.keycode == e.keycode) {
            XNextEvent(display, &event); //remove false event
            return;
        }
    }

    KeySym ks;
    int min, max;
    char typed [4] = {'\0'};
    XLookupString(&e, typed, 1, &ks, 0);
    XDisplayKeycodes(display, &min, &max);

    jeKeyboardKey scan, ksym;

    if (e.keycode-min > 127) {
        scan = ksym = JE_KEY_NONE;
    } else {
        scan = JE_KEYBOARD_OSTOSTD[e.keycode - min];
        ksym = JE_KEYBOARD_OSTOSTD[
            XKeysymToKeycode(display, ks) - min];
    }

    _jeKeyboardSetKeyState(scan, ksym, typed, down);
}

static void buttonEvent(XEvent event, int down)
{
    XButtonEvent e = event.xbutton;
    JEEventMouseButton ev;
    ev.type = JE_EVENT_MOUSE_BUTTON;
    ev.button = e.button;
    ev.down = down;
    ev.x = e.x;
    ev.y = e.y;
    _jeEventSubmit((JEEvent *) &ev);
}

void _jeWindowPollEvents()
{
    pollMouseInput();

    XEvent event;
    while (XPending(display)) {
        XNextEvent(display, &event);

        switch (event.type) {
        case KeyPress:
            keyEvent(event, 1);
            break;
        case KeyRelease:
            keyEvent(event, 0);
            break;
        case MappingNotify:
            XRefreshKeyboardMapping(&event.xmapping);
            break;
        case ClientMessage:
            if (event.xclient.data.l[0] == wmDeleteMessage) {
                jeGameStop();
            }
            break;
        case EnterNotify:
            {
                JEEventWindowEnter ev;
                ev.type = JE_EVENT_WINDOW_ENTER;
                _jeEventSubmit((JEEvent *) &ev);
            }
            break;
        case LeaveNotify:
            {
                JEEventWindowLeave ev;
                ev.type = JE_EVENT_WINDOW_LEAVE;
                _jeEventSubmit((JEEvent *) &ev);
            }
            break;
        case ButtonPress:
            buttonEvent(event, 1);
            break;
        case ButtonRelease:
            buttonEvent(event, 0);
            break;
        case ConfigureNotify:
            {
                if (event.xconfigure.width != width ||
                    event.xconfigure.height != height) {
                    width = event.xconfigure.width;
                    height = event.xconfigure.height;
                    _jeRenderResize(width, height);
                }
            }
            break;
        default:
            jeLoggerDebug("Window event not implemented (%u)", event.type);
        }
    }
}

static void updateWindowProperties()
{
    static int cursorHidden = 0;

    if (flags & JE_WINDOW_GRAB_CURSOR) {
        if (!cursorHidden) {
            XFixesHideCursor(display, window);
            cursorHidden = 1;
        }
    } else {
        if (cursorHidden) {
            XFixesShowCursor(display, window);
            cursorHidden = 0;
        }
    }

    XSizeHints *sizehints = XAllocSizeHints();
    sizehints->flags = 0;
    if (!(flags & (JE_WINDOW_RESIZABLE | JE_WINDOW_FULLSCREEN))) {
        sizehints->min_width = sizehints->max_width = width;
        sizehints->min_height = sizehints->max_height = height;
        sizehints->flags |= (PMaxSize | PMinSize);
    }

    sizehints->x = 0;
    sizehints->y = 0;

    XWMHints *wmhints = XAllocWMHints();
    wmhints->input = 1;
    wmhints->flags = InputHint | StateHint;
    wmhints->initial_state = NormalState;

    XClassHint *classhints = XAllocClassHint();
    classhints->res_class = title;
    classhints->res_name = "Jotunn";

    XTextProperty windowname;
    XStringListToTextProperty(&title, 1, &windowname);

    XSetWMProperties(display, window, &windowname, 0, jeGameGetArgv(),
                     jeGameGetArgc(), sizehints, wmhints, classhints);

    XFree(sizehints);
    XFree(wmhints);
    XFree(classhints);
    XFree(windowname.value);

    XResizeWindow(display, window, width, height);

    XEvent ev;
    ev.type = ClientMessage;
    ev.xclient.message_type = XInternAtom(display, "_NET_WM_STATE", False);
    ev.xclient.format = 32;
    ev.xclient.window = window;
    ev.xclient.data.l[0] = (flags & JE_WINDOW_FULLSCREEN) ? 1 : 0;
    ev.xclient.data.l[1] =
        XInternAtom(display, "_NET_WM_STATE_FULLSCREEN", False);
    ev.xclient.data.l[2] = 0;

    XSendEvent(display, RootWindow(display, screen), 0,
               SubstructureNotifyMask | SubstructureRedirectMask, &ev);
}

void _jeWindowCreate(JEWindowProperties *wp, JEError *err)
{
    width = wp->width;
    height = wp->height;
    flags = wp->flags;
    title = malloc(strlen(wp->title)+1);
    strcpy(title, wp->title);

    display = XOpenDisplay(NULL);
    if (!display) {
        JE_ERROR(err, "Cannot open display");
        return;
    }

    int screen = DefaultScreen(display);

    window = XCreateSimpleWindow(display, RootWindow(display, screen),
                                 0, 0, wp->width, wp->height,
                                 1, BlackPixel(display, screen),
                                 WhitePixel(display, screen));

    if (!window) {
        JE_ERROR(err, "Cannot create window");
        return;
    }

    XSelectInput(display, window,
                 PointerMotionHintMask | KeyPressMask | KeyReleaseMask |
                 ButtonPressMask | ButtonReleaseMask |
                 EnterWindowMask | LeaveWindowMask | StructureNotifyMask |
                 MappingNotify);

    wmDeleteMessage = XInternAtom(display, "WM_DELETE_WINDOW", False);
    XSetWMProtocols(display, window, &wmDeleteMessage, 1);

    XMapWindow(display, window);

    updateWindowProperties();
}

void _jeWindowCleanup()
{
    free(title);
    XDestroyWindow(display, window);
    XCloseDisplay(display);
}

void jeWindowUpdateFlags(long enable, long disable)
{
    flags |= enable;
    flags &= ~disable;

    updateWindowProperties();
}

int jeWindowIsFlagSet(long flag)
{
    return flags & flag;
}

void jeWindowSetTitle(char *ntitle)
{
    free(title);
    title = malloc(strlen(ntitle));
    strcpy(title, ntitle);

    updateWindowProperties();
}

void jeWindowSetDimensions(int nwidth, int nheight)
{
    width = nwidth;
    height = nheight;

    updateWindowProperties();
}

int jeWindowGetWidth()
{
    return width;
}

int jeWindowGetHeight()
{
    return height;
}
