/* Copyright (C) 2017-2018 Sam Bazley and Arnau Bigas
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include "JEModel.h"

#include <string.h>
#include <stdio.h>

#include "JEVec.h"
#include "JEVkUtil.h"
#include "JEFSUtil.h"

#define MODEL_FILE_VERSION 0

#define BIT_ALBEDO_TEXTURE   1
#define BIT_NORMAL_TEXTURE   2
#define BIT_SPECULAR_TEXTURE 4

#define BIT_NORMAL_DATA 1
#define BIT_UV_COORDS   2
#define BIT_MAT_DATA    3

typedef struct JEMaterial {
    jeVec3f diffuse;
    jeVec3f specular;
    float specIntensity;
    unsigned short specHardness;
    float emissivity;
} JEMaterial;

struct _JEModel {
    char *name;
    unsigned char materialCount;
    JEMaterial *materials;
    size_t vertexDataOffset;
    size_t indexDataOffset;
    unsigned int polygonCount;
};

static int modelCount;
static JEModel *models;

static VkBuffer modelDataBuffer;
static VkDeviceMemory modelDataMemory;

static void readString(FILE *f, char *buffer)
{
    char c;
    int pos = 0;
    while ((c = getc(f)) != '\0') {
        buffer[pos++] = c;
    }
    buffer[pos] = '\0';
}

static void loadMaterials(FILE *f, JEModel *mdl)
{
    fread(&mdl->materialCount, sizeof(char), 1, f);

    mdl->materials = malloc(mdl->materialCount * 34);
    for (int i = 0; i < mdl->materialCount; i++) {
        fread(&mdl->materials[i].diffuse, sizeof(jeVec3f), 1, f);
        fread(&mdl->materials[i].specular, sizeof(jeVec3f), 1, f);
        fread(&mdl->materials[i].specIntensity, sizeof(float), 1, f);
        fread(&mdl->materials[i].specHardness, sizeof(short), 1, f);
        fread(&mdl->materials[i].emissivity, sizeof(float), 1, f);
    }
}

static void loadMeshes(FILE *f, JEModel *mdl, size_t *dataSize, void **data)
{
    //Mesh properties
    unsigned char meshProperties;
    fread(&meshProperties, sizeof(char), 1, f);

    //Position vertex data
    unsigned int positionDataCount;
    fread(&positionDataCount, sizeof(int), 1, f);

    size_t positionDataLength = positionDataCount * sizeof(jeVec3f);
    jeVec3f *positionData = malloc(positionDataLength);
    fread(positionData, sizeof(jeVec3f), positionDataCount, f);

    //Normal vertex data
    unsigned int normalDataCount;
    fread(&normalDataCount, sizeof(int), 1, f);

    size_t normalDataLength = normalDataCount * sizeof(jeVec3f);
    jeVec3f *normalData = malloc(normalDataLength);
    fread(normalData, sizeof(jeVec3f), normalDataCount, f);

    //Data vertex indices
    unsigned int vertexIndexCount;
    fread(&vertexIndexCount, sizeof(int), 1, f);

    size_t vertexDataLength = vertexIndexCount * 2 * sizeof(jeVec3f);
    void *vertexData = malloc(vertexDataLength);
    for (unsigned int i = 0; i < vertexIndexCount; i++) {
        int indices[2];
        fread(indices, 2 * sizeof(int), 1, f);

        jeVec3f *vec3 = (jeVec3f*)vertexData;
        vec3[i * 2]     = positionData[indices[0]];
        vec3[i * 2 + 1] = normalData[indices[1]];
    }

    free(positionData);
    free(normalData);

    //Indices (the real ones)
    unsigned int indexCount;
    fread(&indexCount, sizeof(int), 1, f);

    size_t indexDataLength = indexCount * sizeof(int);
    void *indexData = malloc(indexDataLength);
    fread(indexData, sizeof(int), indexCount, f);

    //Set offsets and final polygon count
    mdl->vertexDataOffset = *dataSize;
    mdl->indexDataOffset  = *dataSize + vertexDataLength;
    mdl->polygonCount += indexCount/3;

    //Reallocate data buffer and copy the data
    *dataSize += vertexDataLength + indexDataLength;
    *data = realloc(*data, *dataSize);

    memcpy((*data)+mdl->vertexDataOffset, vertexData, vertexDataLength);
    memcpy((*data)+mdl->indexDataOffset,  indexData,  indexDataLength);

    free(vertexData);
    free(indexData);
}

static void loadModel(const char *filepath, void **data, size_t *dataSize,
                      JEError *err)
{
    //Load model from file, make sure textures exist, etc
    FILE *f = fopen(filepath, "rb");

    if (!f) {
        JE_ERROR(err, "Couldn't find model file %s.", filepath);
        return;
    }

    char magic[3];

    fread(magic, sizeof(char), 3, f);

    if (magic[0] != 'J' || magic[1] != 'E' || magic[2] != 'M') {
        JE_ERROR(err, "Header missmatch for model file %s. "
                 "Is the file corrupted?", filepath);
        return;
    }

    unsigned char version;

    fread(&version, sizeof(char), 1, f);

    if (version > MODEL_FILE_VERSION) {
        JE_ERROR(err, "Model file version is %d, supported version is %d.",
                 version, MODEL_FILE_VERSION);
        return;
    }

    models = realloc(models, ++modelCount * sizeof(JEModel));
    JEModel *mdl = &models[modelCount-1];

    mdl->name = jeFSUtilGetFilename(filepath);

    //buffer used for various string readings
    char buffer[128];
    readString(f, buffer);

    //TODO: associate pipelines with models and stuff

    unsigned char textureInfo;
    fread(&textureInfo, sizeof(char), 1, f);

    //TODO: read texture strings if any

    struct PhysicsInfo {
        float mass;
        float value;
        jeVec3f com;
    } physicsInfo;

    fread(&physicsInfo, sizeof(struct PhysicsInfo), 1, f);

    loadMaterials(f, mdl);

    mdl->polygonCount = 0; //Initialize triangle count
    unsigned char meshCount;
    fread(&meshCount, sizeof(char), 1, f);

    for (int i = 0; i < meshCount; i++) {
        loadMeshes(f, mdl, dataSize, data);
    }
}

void _jeModelLoad(VkDevice dev, VkPhysicalDevice physicalDev, JEError *err)
{
    //Load every model
    size_t dataSize = 0;
    void *data = NULL;

    loadModel("test.jem", &data, &dataSize, err);
    if (jeErrorIsSet(err)) return;

    //Create vulkan buffers and upload data
    VkBufferCreateInfo bufferInfo = {
        VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        NULL,
        0,
        dataSize,
        VK_BUFFER_USAGE_TRANSFER_DST_BIT |
        VK_BUFFER_USAGE_INDEX_BUFFER_BIT |
        VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
        VK_SHARING_MODE_EXCLUSIVE,
        0,
        NULL
    };

    VkResult res = vkCreateBuffer(dev, &bufferInfo, NULL, &modelDataBuffer);
    if (res != VK_SUCCESS) {
        JE_VKERROR(err, "Couldn't create model data buffer", res);
        return;
    }

    VkMemoryRequirements memReq;
    vkGetBufferMemoryRequirements(dev, modelDataBuffer, &memReq);

    modelDataMemory = jeVkUtilAllocateMemory(dev, memReq,
                                            VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                                            err);
    if (jeErrorIsSet(err)) return;

    res = vkBindBufferMemory(dev, modelDataBuffer, modelDataMemory, 0);
    if (res != VK_SUCCESS) {
        JE_VKERROR(err, "Couldn't bind model data buffer to memory", res);
    }

    jeVkUtilStageData(dev, physicalDev, modelDataBuffer, dataSize, data, err);
    if (jeErrorIsSet(err)) return;

    free(data);
}

JEModel *jeModelGet(const char *name)
{
    for (int i = 0; i < modelCount; i++) {
        if (strcmp(models[i].name, name) == 0) return &models[i];
    }
    return NULL;
}

static void bindBuffers(VkCommandBuffer cmdBuffer, JEModel *model)
{
    int VERTEX_BUFFER_BIND_ID = 0;//something something pipeline etc
    vkCmdBindVertexBuffers(cmdBuffer, VERTEX_BUFFER_BIND_ID, 1,
                           &modelDataBuffer, &model->vertexDataOffset);
    vkCmdBindIndexBuffer(cmdBuffer, modelDataBuffer,
                         model->indexDataOffset, VK_INDEX_TYPE_UINT32);
}

void jeModelDraw(JECommandBuffer *cmdBuffer, JEModel *model)
{
    bindBuffers(cmdBuffer->vkHandle, model);
    vkCmdDrawIndexed(cmdBuffer->vkHandle, model->polygonCount*3, 1, 0, 0, 0);
    cmdBuffer->stats.totalPolygons += model->polygonCount;
    cmdBuffer->stats.drawCalls++;
    cmdBuffer->stats.drawnObjects++;
}

void jeModelDrawInstanced(JECommandBuffer *cmdBuffer, JEModel *model,
                          unsigned int instanceCount, VkBuffer instanceBuffer,
                          size_t offset)
{
    int INSTANCE_BUFFER_BIND_ID = 0;//something something pipeline etc
    bindBuffers(cmdBuffer->vkHandle, model);
    vkCmdBindVertexBuffers(cmdBuffer->vkHandle, INSTANCE_BUFFER_BIND_ID, 1,
                           &instanceBuffer, &offset);
    vkCmdDrawIndexed(cmdBuffer->vkHandle, model->polygonCount, instanceCount, 0,
                     0, 0);
    cmdBuffer->stats.totalPolygons += model->polygonCount * instanceCount;
    cmdBuffer->stats.drawCalls++;
    cmdBuffer->stats.drawnObjects += instanceCount;
}

void _jeModelCleanup(VkDevice dev)
{
    vkFreeMemory(dev, modelDataMemory, NULL);
    vkDestroyBuffer(dev, modelDataBuffer, NULL);
}
